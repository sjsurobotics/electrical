#include "Serial_Port.h"

#include <memory>
#include <iostream>
#include <string>
#include <cstring>
#include <cctype>
#include <map>
#include <limits>
#include <sstream>

void print_commands();
void execute_command(std::size_t com);
void shutdown_immediate();
void shutdown_gradual(std::size_t delay);
void disable_converter(std::size_t converter, std::size_t delay);
void enable_converter(std::size_t converter, std::size_t delay);
void request_info(std::size_t info_sel);
void reset_capacity();
void dump_eeprom();
void transmit_to_controller(std::uint_fast8_t com); // Transmit and log to stdout


using SerCom = std::unique_ptr<RS_232::Serial_Port>;
SerCom port;

void flush_uart_buffer(const SerCom&);

const std::size_t com_bit_size = 3;

int main(int argc, char* argv[]){
    if(argc < 3){
        std::clog
            << "Usage: com_test [COM#] [Baud Rate]\r\n"
            << "\tWindows only.\r\n"
            ;
        return 0;
    }

    std::string arghold(argv[1]);
    if(arghold.size() != 4 || arghold.substr(0, 3) != "COM"){
        std::cerr << "Error! Only COM ports for Windows are accepted.\r\n";
        return 0;
    }
    std::stringstream ss;
    ss << argv[2];
    std::size_t baud_rate = 0;
    ss >> baud_rate;
    port.reset(RS_232::open_serial_port(
        arghold.back() - '0',
        static_cast<RS_232::Serial_Port::baud_rate>(baud_rate)
        ));
    if(!port->is_connected()){
        std::cerr << "No device found" << std::endl;
        return 0;
    }

    const std::map<std::string,std::size_t> com_str_list({
        {"help",'h'}, {"shutdown",'2'}, {"gradual",'3'}, {"disable",'4'},
        {"enable",'5'}, {"request",'6'}, {"read",'~'}, {"startup",'7'},
        {"reset capacity",'8'}
        });

    print_commands();
    while(true){
        flush_uart_buffer(port);

        std::cout << "Command: ";
        std::string com;
        std::getline(std::cin, com);
        for(auto& ch : com) ch = std::tolower(ch);
        if(com.size() == 1){
            if(com.front() == 'q')  return 0;
            if(com.front() == 'f')  continue;
            execute_command(com.front());
        } else {
            for(const auto& pair : com_str_list){
                if(pair.first == com){
                    execute_command(pair.second);
                    break;
                }
            }
        }
    }

    port->close();

    return 0;
}

void print_commands(){
    std::cout << "Available commands:\r\n"
        << "\tQ - Quit\r\n"
        << "\tH or Help - Print this list of commands\r\n"
        << "\t0x2 or Shutdown - Disable all converters immediately\r\n"
        << "\t0x3 or Gradual [N] - Disable all converters after N seconds\r\n"
        << "\t0x4 or Disable [C] [N] - Disable converter C (0 - 3) after N seconds\r\n"
        << "\t0x5 or Enable [C] [N] - Enable converter C (0 - 3) after N seconds\r\n"
        << "\t0x6 or Request [CH] - Request information CH\r\n"
            << "\t\t0 - All information\r\n"
            << "\t\t1 - Time passed\r\n"
            << "\t\t2 - State of Charge\r\n"
            << "\t\t3 - Capacity left\r\n"
            << "\t\t4 - Voltage\r\n"
            << "\t\t5 - Current\r\n"
            << "\t\t6 - Temperature\r\n"
        << "\t0x7 or Startup [N] - Enable each converter after delay N seconds\r\n"
        << "\t0x8 or Reset Capacity - Reset capacity to max\r\n"
        << "\t~ or Read - Read the last data logged to the EEPROM\r\n"
        << "\tf - Flush UART buffer\r\n"
        ;
}

void execute_command(std::size_t com){
    auto clear_stream = [](){
        if(!std::cin.good()){
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            return true;
        }
        return false;
        };
    std::size_t arg = 0;
    switch(com){
        case 'h':
            print_commands();
            break;
        case '2':
            shutdown_immediate();
            break;
        case '3':
            std::cout << "Delay: ";
            std::cin >> com;
            if(clear_stream()){
                std::cerr << "Invalid number\r\n";
                break;
            }
            shutdown_gradual(com);
            break;
        case '4':
            std::cout << "Converter: ";
            std::cin >> com;
            if(clear_stream()){
                std::cerr << "Invalid number\r\n";
                break;
            }
            std::cout << "Delay: ";
            std::cin >> arg;
            if(clear_stream()){
                std::cerr << "Invalid number\r\n";
                break;
            }
            disable_converter(com, arg);
            break;
        case '5':
            std::cout << "Converter: ";
            std::cin >> com;
            if(clear_stream()){
                std::cerr << "Invalid number\r\n";
                break;
            }
            std::cout << "Delay: ";
            std::cin >> arg;
            if(clear_stream()){
                std::cerr << "Invalid number\r\n";
                break;
            }
            enable_converter(com, arg);
            break;
        case '6':
            std::cout << "Information type: ";
            std::cin >> com;
            if(clear_stream()){
                std::cerr << "Invalid type\r\n";
                break;
            }
            request_info(com);
            break;
        case '7':
            std::cout << "Enabling everything. Delay: ";
            std::cin >> com;
            if(clear_stream()){
                std::cerr << "Invalid type\r\n";
                break;
            }
            enable_converter(0, com);
            enable_converter(1, com);
            enable_converter(2, com);
            enable_converter(3, com);
            break;
        case '8':
            std::cout << "Resetting battery capacity to max.\n";
            reset_capacity();
            break;
        case '~':
            dump_eeprom();
            break;
        default:
            std::cerr << "Invalid command!\r\n";
            break;
    }
}

void shutdown_immediate(){
    transmit_to_controller(0x2);
}

void shutdown_gradual(std::size_t delay){
    transmit_to_controller(0x3 | (delay << com_bit_size));
}

void disable_converter(std::size_t converter, std::size_t delay){
    transmit_to_controller(
        0x4
        | ((delay & 0x7) << com_bit_size)
        | (converter  << (com_bit_size + 3))
        );
}

void enable_converter(std::size_t converter, std::size_t delay){
    transmit_to_controller(
        0x5
        | ((delay & 0x7) << com_bit_size)
        | (converter  << (com_bit_size + 3))
        );
}

void request_info(std::size_t info_sel){
    transmit_to_controller(0x6 | (info_sel << (com_bit_size + 1)));
}

void reset_capacity(){
    transmit_to_controller(0x7);
}

void dump_eeprom(){
    transmit_to_controller(0x6 | (0xF << (com_bit_size + 1)));
}

void flush_uart_buffer(const SerCom& port){
    std::size_t available = port->available();
    if(available > 1){
        char* buf = new char[available+1];
        port->read(buf, available);
        buf[available] = '\0';

        auto print_data = [buf](
            std::string&& data_label, std::string&& unit,
            char delim,
            std::size_t& j, bool ignore
        ){
            char* end_str = std::strchr(buf+j, delim);
            if(end_str == nullptr) return;
            std::cout
                << "\t" << data_label << ": "
                << std::string(buf+j+ignore, end_str)
                << " " << unit << std::endl
                ;
            j = end_str - buf;
        };

        std::cout << "Raw: " << buf << "\nInterpreted:\n" << std::endl;
        for(std::size_t i = 0; i < available; ++i){
            switch(buf[i]){
                case 't':
                    print_data("Time passed", "s", '\r', i, true);
                    break;
                case 'S':
                    print_data("State of Charge", "%", '\r', i, true);
                    break;
                case 'C':
                    print_data("Capacity Left", "mAh", '\r', i, true);
                    break;
                case 'V':
                    print_data("Voltage", "V", '\r', i, true);
                    break;
                case 'I':
                    print_data("Current", "mA", '\r', i, true);
                    break;
                case 'T':
                    print_data("Temperature", "degrees Celsius", '\r', i, true);
                    break;
                case 'B':
                    print_data("", "", '~', i, false);
                    break;
                default: break;
            }
        }

        delete buf;
    }
    std::cout << std::endl;
}

void transmit_to_controller(std::uint_fast8_t com){
    std::cout << "\nCommand sent: ";    // Print partial to help indicate any hang ups
    port->write(com);
    std::cout << "0x" << std::hex << static_cast<int>(com) << '\n';
}