#include <cstdint>

#define BUFFER_MAX_SIZE 10

typedef std::uint8_t byte;

struct {
    byte
        c0 : 1,
        c1 : 1,
        c2 : 1,
        c3 : 1
        ;
} enable_flags;

    // UART-related variables
#define BUFFER_MAX_SIZE 10
byte ser_buf[BUFFER_MAX_SIZE];
byte buf_sz = 0;    // Hold the actual number of bytes read

void setup(){
    enable_flags.c0 = false;
    enable_flags.c1 = false;
    enable_flags.c2 = false;
    enable_flags.c3 = false;

    pinMode(2, OUTPUT);
    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT);

    pinMode(7, INPUT);

    SerialUSB.begin(115200);  // Initialize SerialUSB Monitor USB
    while(!SerialUSB);
}

void loop(){
    while (SerialUSB.available()) // If data is sent to the monitor
    {
        buf_sz = SerialUSB.readBytes(ser_buf, BUFFER_MAX_SIZE);
        for(unsigned i = 0; i < buf_sz; ++i){
            switch(ser_buf[i]){
                case '0':
                    enable_flags.c0 ^= 0x1;
                    break;
                case '1':
                    enable_flags.c1 ^= 0x1;
                    break;
                case '2':
                    enable_flags.c2 ^= 0x1;
                    break;
                case '3':
                    enable_flags.c3 ^= 0x1;
                    break;
            }
        }
        digitalWrite(2, enable_flags.c0);
        digitalWrite(3, enable_flags.c1);
        digitalWrite(4, enable_flags.c2);
        digitalWrite(5, enable_flags.c3);
        SerialUSB.print("Emergency Button Pressed: ");
            SerialUSB.println(digitalRead(7) ? "TRUE" : "FALSE");
            SerialUSB.print("\tConverter 0 status: ");
            SerialUSB.println(enable_flags.c0 ? "ON" : "OFF");
            SerialUSB.print("\tConverter 1 status: ");
            SerialUSB.println(enable_flags.c1 ? "ON" : "OFF");
            SerialUSB.print("\tConverter 2 status: ");
            SerialUSB.println(enable_flags.c2 ? "ON" : "OFF");
            SerialUSB.print("\tConverter 3 status: ");
            SerialUSB.println(enable_flags.c3 ? "ON" : "OFF");
    }
}