/*******************************************************************************

                                Connection test

        Used to verify proper connection to digital and analog peripherals
    for the Power Controller Board.

********************************************************************************/



/***************************************************************************
                            Global variables Start
***************************************************************************/
    // Digital Enable pins
const byte enable_pin[4] = {2, 3, 4, 5};
    // Digital input pin for the emergency button
const byte emergency_button_pin = 7;
    // Analog input pins
const byte
    voltage_pin = 3,    // Error: 10 (raw)  --> 0.06 V
    current2_pin = 2,   // Error: 18 (raw)  --> 0.44 A
    current_pin = 1,    // Error: 19 (raw)  --> 2.78 A
    temp_pin = 0        // Error: 19 (raw)  --> 1.53 C
    ;

uint32_t led_counter = 0;

const byte dac_pin = A0;
uint16_t dac_counter = 0;
float
    avg_err_volt_raw = 0, last_volt = 0, max_err_volt_raw = 0, max_err_volt = 0,
    avg_err_cur2_raw = 0, last_cur2 = 0, max_err_cur2_raw = 0, max_err_cur2 = 0,
    avg_err_curr_raw = 0, last_curr = 0, max_err_curr_raw = 0, max_err_curr = 0,
    avg_err_temp_raw = 0, last_temp = 0, max_err_temp_raw = 0, max_err_temp = 0
    ;

/***************************************************************************
                            Global variables End
***************************************************************************/



/***************************************************************************
                        Mapping functions start
***************************************************************************/
    // Values are adjusted due to imperfect voltage divider
#define ADC_VAL_MAX 4107.254
#define VREF_CORR   3.309067

#define ERR_ADC         40
#define TEMP_ERR_V      0.04
#define CURR_ERR_COMP   0.11
#define CUR2_ERR_COMP   -0.08

#define ADC_TO_VOLTAGE_SCALE(WORD) ((WORD-ERR_ADC) * 26.4/ADC_VAL_MAX)
#define ADC_TO_CURRENT_SCALE(WORD) ((WORD-ERR_ADC)*600/ADC_VAL_MAX)
#define ADC_TO_CURRENT2_SCALE(WORD) ((WORD-ERR_ADC)*100/ADC_VAL_MAX)
#define ADC_TO_TEMPERATURE_SCALE(WORD) ((WORD*VREF_CORR/ADC_VAL_MAX + TEMP_ERR_V)/0.01)

#define ADC_TO_VOLTAGE(WORD) ADC_TO_VOLTAGE_SCALE(WORD)
#define ADC_TO_CURRENT(WORD) (ADC_TO_CURRENT_SCALE(WORD) - 300 + CURR_ERR_COMP)
#define ADC_TO_CURRENT2(WORD) (ADC_TO_CURRENT2_SCALE(WORD) - 50 + CUR2_ERR_COMP)
#define ADC_TO_TEMPERATURE(WORD) (ADC_TO_TEMPERATURE_SCALE(WORD) - 39.6)
/***************************************************************************
                        Mapping functions end
***************************************************************************/



void setup(void){
    analogReadResolution(12);

    pinMode(emergency_button_pin, INPUT);
    for(unsigned short i = 0; i < 4; ++i){
        pinMode(enable_pin[i], OUTPUT);
        digitalWrite(enable_pin[i], HIGH);
    }

    SerialUSB.begin(9600);

    analogWriteResolution(10);
}

void loop(void){
    bool emergency_pressed = digitalRead(emergency_button_pin);
    if(!emergency_pressed){
        for(unsigned short i = 0; i < 4; ++i)
            digitalWrite(enable_pin[i], (led_counter%4u) == i);
        float hold = analogRead(voltage_pin);
        SerialUSB.print("Voltage at A3: ");
            last_volt = hold;
            SerialUSB.print(hold);
            SerialUSB.print("\t\"Voltage\" is: ");
            hold = ADC_TO_VOLTAGE(hold);
                SerialUSB.print(hold);
                SerialUSB.println(" V");
        SerialUSB.print("Voltage at A1: ");
            last_curr = hold = analogRead(current_pin);
            SerialUSB.print(hold);
            SerialUSB.print("\t\"Current\" is: ");
            hold = ADC_TO_CURRENT(hold);
                SerialUSB.print(hold);
                SerialUSB.println(" A");
        SerialUSB.print("Voltage at A2: ");
            last_cur2 = hold = analogRead(current2_pin);
            SerialUSB.print(hold);
            SerialUSB.print("\t\"Current\" is: ");
            hold = ADC_TO_CURRENT2(hold);
                SerialUSB.print(hold);
                SerialUSB.println(" A");
        SerialUSB.print("Voltage at A0: ");
            last_temp = hold = analogRead(temp_pin);
            SerialUSB.print(hold);
            SerialUSB.print("\t\"Temperature\" is: ");
            hold = ADC_TO_TEMPERATURE(hold);
                SerialUSB.print(hold);
                SerialUSB.println(" degrees C");

        max_err_volt_raw = max_err_cur2_raw = max_err_curr_raw = max_err_temp_raw = 0.0;
    } else if(led_counter > 0) {
        float volt = analogRead(voltage_pin);
        float cur2 = analogRead(current2_pin);
        float curr = analogRead(current_pin);
        float temp = analogRead(temp_pin);
        float err_volt = (volt-last_volt);
        float err_cur2 = (cur2-last_cur2);
        float err_curr = (curr-last_curr);
        float err_temp = (temp-last_temp);
        avg_err_volt_raw = (err_volt + avg_err_volt_raw)/2.0f;
        avg_err_cur2_raw = (err_cur2 + avg_err_cur2_raw)/2.0f;
        avg_err_curr_raw = (err_curr + avg_err_curr_raw)/2.0f;
        avg_err_temp_raw = (err_temp + avg_err_temp_raw)/2.0f;
        last_volt = volt;
        last_cur2 = cur2;
        last_curr = curr;
        last_temp = temp;
        if(err_volt > max_err_volt_raw) max_err_volt_raw = err_volt;
        if(err_cur2 > max_err_cur2_raw) max_err_cur2_raw = err_cur2;
        if(err_curr > max_err_curr_raw) max_err_curr_raw = err_curr;
        if(err_temp > max_err_temp_raw) max_err_temp_raw = err_temp;

        SerialUSB.print("Average Voltage error: ");
            SerialUSB.print(avg_err_volt_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_VOLTAGE_SCALE(avg_err_volt_raw));
            SerialUSB.println(" V");
            SerialUSB.print("\tMax error: ");
            SerialUSB.print(max_err_volt_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_VOLTAGE_SCALE(max_err_volt_raw));
            SerialUSB.println(" V");
        SerialUSB.print("Average Current 2 error: ");
            SerialUSB.print(avg_err_cur2_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_CURRENT2_SCALE(avg_err_cur2_raw));
            SerialUSB.println(" A");
            SerialUSB.print("\tMax error: ");
            SerialUSB.print(max_err_cur2_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_CURRENT2_SCALE(max_err_cur2_raw));
            SerialUSB.println(" A");
        SerialUSB.print("Average Current error: ");
            SerialUSB.print(avg_err_curr_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_CURRENT_SCALE(avg_err_curr_raw));
            SerialUSB.println(" A");
            SerialUSB.print("\tMax error: ");
            SerialUSB.print(max_err_curr_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_CURRENT_SCALE(max_err_curr_raw));
            SerialUSB.println(" A");
        SerialUSB.print("Average Temperature error: ");
            SerialUSB.print(avg_err_temp_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_TEMPERATURE_SCALE(avg_err_temp_raw));
            SerialUSB.println(" degrees C");
            SerialUSB.print("\tMax error: ");
            SerialUSB.print(max_err_temp_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_TEMPERATURE_SCALE(max_err_temp_raw));
            SerialUSB.println(" degrees C");
    } else {
        last_volt = analogRead(voltage_pin);
        last_cur2 = analogRead(current2_pin);
        last_curr = analogRead(current_pin);
        last_temp = analogRead(temp_pin);
    }

    analogWrite(dac_pin, dac_counter);

    ++led_counter;
    dac_counter += 10;
    if(dac_counter >= 1024) dac_counter = 0;
    delay(50);
}