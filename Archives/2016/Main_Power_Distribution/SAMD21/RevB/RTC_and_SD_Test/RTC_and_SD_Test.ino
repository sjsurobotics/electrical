/***************************************************************************

                    RTC and SD Card Communication Test

    Description: Test the timing capabilities of the Real Time Clock for
        logging and time stamping logs. Also test the capability to read
        and write to a micro SD card using SPI.

***************************************************************************/






/***************************************************************************
                            Dependencies Start
***************************************************************************/
#include <RTCZero.h>
#include <SD.h>

#include <cstdlib>
#include <cstdint>
#include <cstring>
/***************************************************************************
                            Dependencies End
***************************************************************************/



/***************************************************************************
                            Type Aliases Start
***************************************************************************/

    // Event codes
typedef byte event_code;
#define NO_EVENT    0x0
#define IMME_SHDN   0x1
#define GRAD_SHDN   0x2
#define OVERCURRENT 0x3
#define OVERVOLT    0x4
#define OVERTEMP    0x5
#define EMERGENCY   0x6
#define LOW_CAP     0x7
#define CONV0EN     0x8
#define CONV1EN     0x9
#define CONV2EN     0xA
#define CONV3EN     0xB
#define CONV0DIS    0xC
#define CONV1DIS    0xD
#define CONV2DIS    0xE
#define CONV3DIS    0xF

#define dword std::uint32_t
#define size_type dword

    // Compact POD structure for measurement information
struct Battery_Data {
    dword
        time : 16,
        SoC : 8,
        capacity : 20,  // Note that by using dword, 1 byte will be
                        //  added as padding before capacity.
        volt : 12,
        curr : 12,
        temp : 12,
        event : 8
        ;
};
/***************************************************************************
                            Type Aliases End
***************************************************************************/


/***************************************************************************
                                Macros start
***************************************************************************/
#define DATA_BYTE_SIZE sizeof(Battery_Data)

    // Logging related macros
#define SD_SEL_PIN 10
#define SD_DET_PIN 20
/***************************************************************************
                                Macros end
***************************************************************************/



/***************************************************************************
                            Global variables Start
***************************************************************************/

    // Bit flags and booleans
union {
    struct {
        byte
            start_log : 1,
            timing_state : 1,
            initiate_shutdown : 1,
            initiate_startup : 1,
            sd_detect : 1,
            sd_rdy : 1
            ;
    };
    byte reg;
} bit_flags;

    // Logging and measurement-related variables
RTCZero clock;
char const * const load_file_path = "CAP.dat";
char const * const log_file_path = "LOG.dat";
Battery_Data battery;
event_code last_event = NO_EVENT;
float capacity_left = 0; // Store the capacity in a separate
                                    //  entity that can keep the precision
/***************************************************************************
                            Global variables End
***************************************************************************/



/***************************************************************************
                        Function prototypes Start
    * Address_Select objects are passed by copy since they are small PODs and
        are likely to be similar to the size of a pointer.
***************************************************************************/

    // Logging I/O start
void log_battery_data();    // Address is updated to point to the
                            //  next byte after the newly written
                            //  information.
                            //  Write the entire structure to the
                            //  EEPROM. Since this is a POD, this
                            //  should be as close as possible to
                            //  the individual members.
    // SD card I/O start
void init_sd();

/***************************************************************************
                        Function prototypes End
***************************************************************************/


void setup(){
    SerialUSB.begin(9600);  // Initialize SerialUSB Monitor USB

    while(!SerialUSB);

    pinMode(SD_DET_PIN, INPUT);
    bit_flags.sd_detect = !digitalRead(SD_DET_PIN);

    bit_flags.sd_rdy = false;


    init_sd();
    battery.capacity = capacity_left;
    battery.SoC = 55;
        // Set other initial values
    battery.volt = 4095;
    battery.curr = 12;
    battery.temp = 64;
    battery.event = EMERGENCY;


        // Set up interrupt-related functions
    clock.begin();
    clock.setDate(1, 1, 16);
    clock.setTime(0, 0, 0);
    clock.setAlarmSeconds(0);
    clock.enableAlarm(clock.MATCH_SS);
    clock.attachInterrupt(log_battery_data);
}

void loop(){
    if(digitalRead(SD_DET_PIN) & bit_flags.sd_detect){
        SerialUSB.println("SD card disconnected!");
        bit_flags.sd_detect = false;
        bit_flags.sd_rdy = false;
    } else if(!digitalRead(SD_DET_PIN) & !bit_flags.sd_detect){
        SerialUSB.println("SD card connected!");
        bit_flags.sd_detect = true;
        init_sd();
    }
}



/***************************************************************************
                            Logging I/O start
***************************************************************************/
void log_battery_data(){
    if(!bit_flags.sd_rdy) return;
    SerialUSB.println("Logging next block...");

        // Log static data (last known capacity)
    File log_file = SD.open(load_file_path, FILE_WRITE);
    if(!log_file) SerialUSB.println("\tStatic file was not opened!");
    log_file.seek(0);
    log_file.print(capacity_left);
    log_file.close();

        // Log the last measurements taken to the next block
    log_file = SD.open(log_file_path, FILE_WRITE);
    if(!log_file) SerialUSB.println("\tLog file was not opened!");
        log_file.print("[ D");
        log_file.print(clock.getDay());
        log_file.print(" M");
        log_file.print(clock.getMonth());
        log_file.print(" Y");
        log_file.print(clock.getYear());
        log_file.print(" H");
        log_file.print(clock.getHours());
        log_file.print(" m");
        log_file.print(clock.getMinutes());
        log_file.print(" S");
        log_file.print(clock.getSeconds());
        log_file.print(" ]: ");
    byte data_buf[DATA_BYTE_SIZE+1];
    std::memcpy(data_buf, reinterpret_cast<byte*>(&battery), DATA_BYTE_SIZE);
    data_buf[DATA_BYTE_SIZE] = '\0';
        log_file.write(data_buf, DATA_BYTE_SIZE);
        log_file.println("");
    log_file.close();
        SerialUSB.print("\tCompleted log at [ D");
            SerialUSB.print(clock.getDay());
            SerialUSB.print(" M");
            SerialUSB.print(clock.getMonth());
            SerialUSB.print(" Y");
            SerialUSB.print(clock.getYear());
            SerialUSB.print(" H");
            SerialUSB.print(clock.getHours());
            SerialUSB.print(" m");
            SerialUSB.print(clock.getMinutes());
            SerialUSB.print(" S");
            SerialUSB.print(clock.getSeconds());
            SerialUSB.println(" ]: ");
    --capacity_left;
}
/***************************************************************************
                            Logging I/O end
***************************************************************************/


/***************************************************************************
                            SD Card I/O start
***************************************************************************/
void init_sd(){
    bit_flags.sd_rdy = SD.begin(SD_SEL_PIN) & bit_flags.sd_detect;
        SerialUSB.print("SD card is ");
            SerialUSB.print(bit_flags.sd_rdy ? "" : "not ");
            SerialUSB.println("ready!");


    if(bit_flags.sd_rdy){
            // Attempt to read the last recorded capacity value
        File static_file = SD.open(load_file_path, FILE_READ);
        if(static_file.available()){
            char* num_buf = new char[static_file.available()+1];
            std::size_t i = 0;
            while(static_file.available()){
                num_buf[i++] = static_file.read();
            }
            num_buf[i] = '\0';
            capacity_left = std::strtod(num_buf, NULL);
            delete num_buf;
        } else {
            capacity_left = 0;
        }
    } else {
        capacity_left = 0;
    }
}
/***************************************************************************
                            SD Card I/O end
***************************************************************************/
