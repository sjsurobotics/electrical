/***************************************************************************

                            Power Controller Program

    Description: Track various information about the main battery
        pack of the Labradaor Mk. II. Determine the approximate capacity
        left in the battery pack and safely shut-off power converters
        when needed. Communicate with the main controller, and inform it
        of important information.
        Uses both C and C++.



    Priorities:
        1) Shut off all power converters in the case of a failure
            or another event.
            a) Allow manual shut-off via an emergency button in the
                case all power is lost, including for the
                Power Controller.
            b) While the rover is in operation, continuously provide
                a high signal to keep the power converters enabled.
        2) Communicate with the main controller.
            a) Pass on measured information if requested.
                See command format below.
            b) If commanded so, disable the power converters
                immediately.
            c) If the Power Controller takes the initiative to disable
                the power converters, inform the main controller
                if possible. Specifically, the Power Controller will
                emit "E2" for every converter before it is
                gradually disabled.
        3) Take temperature, current, and/or voltage measurements
            of the battery pack to calculate the approximate
            capacity left.
        4) Record measurements to an external EEPROM.



    UART communication with the main controller:

        Command format:
            Bits:    7 - 3       2 - 0
            Name: [ Options ] [ Command ]

        Available commands from main controller to Power Controller:
          Byte to send  |     Name      |        Description        |          Option Format
        -----------------------------------------------------------------------------------------------
              0x2       |   Shdn. Im.   |       Disable all         |              None
                        |               |   converters immediately. |
              0x3       |  Shdn. Grad.  |       Disable converters  |             7 - 3
                        |               |   one by one gradually.   |       [Delay in seconds]
              0x4       |    Disable    |       Disable a specific  |     7 - 6         5 - 3
                        |               |   converter. The most     |  [Converter] [Delay in seconds]
                        |               |   significant two bits    |
                        |               |   determine which.        |
              0x5       |    Enable     |       Enable a specific   |     7 - 6         5 - 3
                        |               |   converter. The most     |  [Converter] [Delay in seconds]
                        |               |   significant two bits    |
                        |               |   determine which.        |
              0x6       |    Request    |       Requests specific   |              7 - 4
                        |               |   information. The most   | [Type of informaion (see below)]
                        |               |   significant four bits   |
                        |               |   determine which.        |

        When sending the request command, the most significant four
            bits specify which type of information the power
            controller should send back.
                      Hex # |      Name
                    ---------------------------
                       0x0  |       All
                       0x1  |   Time passed
                       0x2  | State of Charge
                       0x3  |  Capacity Left
                       0x4  |     Voltage
                       0x5  |     Current
                       0x6  |   Temperature
        

        The type of data sent back based on the high four bits sent
            with the Request command (Format - [Character][Number]):
          Hex # |      Name       | Char. | Bit size |     Unit
        --------------------------------------------------------------
           0x0  |       All       |       |    88    |
           0x1  |   Time passed   |   t   |    16    |       s
           0x2  | State of Charge |   S   |     8    |  25/64 %/bit
           0x3  |  Capacity Left  |   C   |    32    |      mAh
           0x4  |     Voltage     |   V   |    16    |  25/4 mV/bit
           0x5  |     Current     |   I   |    16    | 9375/64 mA/bit
           0x6  |   Temperature   |   T   |    16    | 125/1024 C/bit
        For example, if the main controller sends 0x36 and the
            capacity left is 15000 mAh, the Power Controller will respond
            with "C0005DC" (0005DC is in hexadecimal).

        Special notes:
            - The Bit size column in the table directly above does not
                include the bits for the character.
            - Information about Capacity is in reality only 20 bits
            - Information about Voltage, Current, and
                Temperature are in reality only 12 bits long.
            - 0x0 will request all information, as if the commands
                0x16, 0x26, 0x36, 0x46, 0x56, and 0x66 were sent
                consecutively.
            - 0x3 will delay the shutdown of each converter by the
                same delay one at a time. The main controller will be
                responsible if there is a need to delay only once
                before shutting down all converters at the same time.



    Emergency messages to the main controller:

        In the event of a failure or another event which causes this
    controller to force shutdown all converters, a message may be
    transmitted to the main controller. If the shutdown is immediate,
    disabling converters will take priority over alerting the main
    controller. However, if the converters will be disabled gradually,
    i.e. with a delay, one of the following messages will be transmitted
    to the main controller:
        - "eV": Undervoltage detected
        - "eT": Over temperature detected
        - "eI": Over current detected
        - "eC": Low capacity detected
            * This low capacity is defined as the capacity when damage
                to the battery pack would occur under continued operation.
        - Note that all emergency shutdown messages can be
            identified with "e" as the starting character.




    I2C communication with a Microchip 24FC1026 (1 Mibit) EEPROM:

        EEPROM record format in order (88 bits in total):
            - 16 bits: Time passed in startup [seconds]
            - 8 bits: State of Charge [25/64 %/bit]
            - 20 bits: Remaining Capacity [mAh]
            - 12 bits: Pack Voltage [25/4 mV/bit]
            - 12 bits: Current [9375/64 mA/bit]
            - 12 bits: Temperature [125/1024 C/bit]
            - 8 bits: Event code
                * 0x0 - Nothing
                * 0x1 - Immediate Shutdown
                * 0x2 - Gradual Shutdown
                * 0x3 - Overcurrent detected
                * 0x4 - Under/Overvoltage detected
                * 0x5 - Over-temperature detected
                * 0x6 - Emergency button pressed
                * 0x7 - Low capacity
                * 0x8 - Converter 0 enabled
                * 0x9 - Converter 1 enabled
                * 0xA - Converter 2 enabled
                * 0xB - Converter 3 enabled
                * 0xC - Converter 0 disabled
                * 0xD - Converter 1 disabled
                * 0xE - Converter 2 disabled
                * 0xF - Converter 3 disabled

***************************************************************************/



/***************************************************************************
                            Dependencies Start
***************************************************************************/
#include "watch_timer.h"        // Required for setting up timed interrupts
#include "byte_types.h"         // Required for types such as word and size_type

#include <RTCZero.h>
#include <SD.h>

#include <cstring>  // For std::memcpy
#include <cstdlib>  // For std::strtod
#include <cstdio>   // For std::sprintf
/***************************************************************************
                            Dependencies End
***************************************************************************/



/***************************************************************************
                            Type Aliases Start
***************************************************************************/

    // Commands the main controller may send to the Power Controller
typedef byte master_command;
#define SHUTDOWN_IMMEDIATE  0x2
#define SHUTDOWN_GRADUAL    0x3
#define DISABLE             0x4
#define ENABLE              0x5
#define REQUEST             0x6
#define RESET_UC            0xF

    // Event codes
typedef byte event_code;
#define NO_EVENT    0x0
#define IMME_SHDN   0x1
#define GRAD_SHDN   0x2
#define OVERCURRENT 0x3
#define OVERVOLT    0x4
#define OVERTEMP    0x5
#define EMERGENCY   0x6
#define LOW_CAP     0x7
#define CONV0EN     0x8
#define CONV1EN     0x9
#define CONV2EN     0xA
#define CONV3EN     0xB
#define CONV0DIS    0xC
#define CONV1DIS    0xD
#define CONV2DIS    0xE
#define CONV3DIS    0xF

    // Compact POD structure for measurement information
struct Battery_Data {
    dword
        time : 16,
        SoC : 8,
        capacity : 20,  // Note that by using dword, 1 byte will be
                        //  added as padding before capacity.
        volt : 12,
        curr : 12,
        temp : 12,
        event : 8
        ;
};
/***************************************************************************
                            Type Aliases End
***************************************************************************/


/***************************************************************************
                                Macros start
***************************************************************************/
#define DATA_BYTE_SIZE sizeof(Battery_Data)
#define POWER_RAIL_COUNT 4

    // Maximum and minimum ratings
#define MAX_CAPACITY 19600  // The maximum capacity of the battery pack (mAh)
#define MIN_SOC 40          // The minimum State of Charge allowable before
                            //  the Power Controller begins shutting down
                            //  converters (%). This is defined as the
                            //  approximate capacity left when the battery
                            //  pack has reached its lowest allowable voltage.
#define MAX_CURRENT 250e3
#define MIN_VOLTAGE 3724    // Minimum as if read by ADC (24 V)
#define MAX_VOLTAGE 4034    // Maximum as if read by ADC (26 V)
#define MAX_TEMP 1310       // Maximum as if read by ADC (100 C)

#define DEL_T 0.041f // The change in time between analog readings (seconds)
#define ADC_VAL_RANGE 4096

    // Values are adjusted due to imperfect voltage divider
#define ADC_VAL_MAX 4107.254f
#define VREF_CORR   3.309067f

#define ERR_ADC         40
#define TEMP_ERR_V      0.04f
#define CURR_ERR_COMP   0.11f
#define CUR2_ERR_COMP   5.7f

    // Macro functions for mapping
#define ADC_TO_VOLTAGE_SCALE(WORD) ((WORD-ERR_ADC) * 26.4f/ADC_VAL_MAX)
#define ADC_TO_CURRENT_SCALE(WORD) ((WORD-ERR_ADC)*600.0f/ADC_VAL_MAX)
#define ADC_TO_CURRENT2_SCALE(WORD) ((WORD-ERR_ADC)*100.0f/ADC_VAL_MAX)
#define ADC_TO_TEMPERATURE_SCALE(WORD) ((WORD*VREF_CORR/ADC_VAL_MAX + TEMP_ERR_V)/0.01f)

#define ADC_TO_VOLTAGE(WORD) ADC_TO_VOLTAGE_SCALE(WORD)
#define ADC_TO_CURRENT(WORD) (ADC_TO_CURRENT_SCALE(WORD) - 300 + CURR_ERR_COMP)
#define ADC_TO_CURRENT2(WORD) (ADC_TO_CURRENT2_SCALE(WORD) - 50 + CUR2_ERR_COMP)
#define ADC_TO_TEMPERATURE(WORD) (ADC_TO_TEMPERATURE_SCALE(WORD) - 39.6)

#define ADC_TO_CURRENTMA(WORD) (ADC_TO_CURRENT(WORD)*1000.0f)

    // Create macros for enable logic of converters
    //  to make switching between positive and negative logic easier.
#define ENABLE_LOGIC    HIGH
#define DISABLE_LOGIC   LOW

    // Control how fast each power system comes online
#define STARTUP_DELAY 2000

    // Logging related macros
#define DATA_BUF_COUNT 10 // The number of blocks to write at once
#define PADDING_CHAR 24
#define TOTAL_BATBUF_SIZE ((DATA_BYTE_SIZE+PADDING_CHAR)*DATA_BUF_COUNT)
/***************************************************************************
                                Macros end
***************************************************************************/



/***************************************************************************
                            Global variables Start
***************************************************************************/
    // Digital Enable pins
const byte enable_pin[POWER_RAIL_COUNT] = {2, 3, 4, 5};
    // Digital input pin for the emergency button
const byte emergency_button_pin = 7;
    // Digital pinds for working with the SD card
const byte sd_sel_pin = 10, sd_det_pin = 20;
    // Analog input pins
const byte
    voltage_pin = 3,    // Error: 10 (raw)  --> 0.06 V
    current2_pin = 2,   // Error: 18 (raw)  --> 0.44 A
    current_pin = 1,    // Error: 19 (raw)  --> 2.78 A
    temp_pin = 0        // Error: 19 (raw)  --> 1.53 C
    ;

    // Bit flags and booleans
union {
    struct {
        byte
            log_block : 1,
            log_in_progress : 1,
            initiate_shutdown : 1,
            initiate_startup : 1,
            sd_detect : 1,
            sd_rdy : 1
            ;
    };
    byte reg;
} bit_flags;

    // Logging and measurement-related variables
RTCZero clock;
char const * const load_file_path = "CAP.dat";
char const * const log_file_path = "LOG.dat";

Battery_Data battery;
unsigned long micro_ref = 0, micro_curr = 0;
big_size_type ticks_passed = 0;     // Store the number of ticks (each tick is
                                    //  DEL_T seconds) separately for calculations.
float capacity_left = MAX_CAPACITY; // Store the capacity in a separate
                                    //  entity that can keep the precision
event_code last_event = NO_EVENT;
    // Buffer to hold data in preparation for logging.
char battery_data_buf[TOTAL_BATBUF_SIZE+DATA_BYTE_SIZE+PADDING_CHAR];
big_size_type bdb_index = 0;

const size_type second_period = 100;
size_type second_counter = 0;       // Upon matching second_period, take
                                    //  measurements.

    // UART-related variables
#define BUFFER_MAX_SIZE 10
byte ser_buf[BUFFER_MAX_SIZE];
byte buf_sz = 0;    // Hold the actual number of bytes read
/***************************************************************************
                            Global variables End
***************************************************************************/



/***************************************************************************
                        Function prototypes Start
    * Address_Select objects are passed by copy since they are small PODs and
        are likely to be similar to the size of a pointer.
***************************************************************************/
    // UART I/O
void transmit_data(byte info_type);

    // Analog measurements
void measure(); // Update the battery object with analog measurements. (State A)
void calculate();   // State H

    // Digital
void emergency_button_handler();
void attempt_startup();
void shutdown_immediate();      // Disable all power converters.
void shutdown_gradual(dword delays);    // Disable all converters one at
                                        //  a time with delay delays
                                        //  in seconds
    // Disable or enable a specified converter after delay seconds.
void Xable_converter(byte converter, dword delays, byte logic);

    // Logging I/O start
void init_sd();
void log_battery_data();    // Address is updated to point to the
                            //  next byte after the newly written
                            //  information.
                            //  Write the entire structure to the
                            //  EEPROM. Since this is a POD, this
                            //  should be as close as possible to
                            //  the individual members.
void add_log_block();       // Enter a new block of data to the buffer.

/***************************************************************************
                        Function prototypes End
***************************************************************************/


void setup(){
    SerialUSB.begin(115200);  // Initialize SerialUSB Monitor USB

        // Manually use the Tx and Rx leds to indicate data transfer
    pinMode(PIN_LED_RXL, OUTPUT);
    pinMode(PIN_LED_TXL, OUTPUT);
    digitalWrite(PIN_LED_RXL, HIGH);    // These leds use active low logic,
    digitalWrite(PIN_LED_TXL, HIGH);    //  so make sure to turn them off.

        // Initialize the bitflags
    bit_flags.log_block = false;
    bit_flags.log_in_progress = false;
    bit_flags.initiate_shutdown = false;
    bit_flags.initiate_startup = false;

        // Initialize battery data and initialize SD card
    pinMode(sd_det_pin, INPUT);
    bit_flags.sd_detect = !digitalRead(sd_det_pin);
    battery.time = 0;
    init_sd();  // Also sets bit_flags.sd_rdy
    battery.capacity = capacity_left;
    battery.SoC = 100*capacity_left/MAX_CAPACITY;
        // Set other initial values
    battery.volt = 4095;
    battery.curr = 0;
    battery.temp = 0;
    battery.event = 0x0;

        // Set up digital pins and ADC
    for(size_type i = 0; i < POWER_RAIL_COUNT; ++i){
        pinMode(enable_pin[i], OUTPUT);
        digitalWrite(enable_pin[i], DISABLE_LOGIC);
    }
    pinMode(emergency_button_pin, INPUT);
        // Enable just the five volt rail
    attempt_startup();
    analogReadResolution(12); // Set up resolution for all analog measurements

        // Set up interrupt-related functions
    configure_watch_timer(TC_CTRLA_PRESCALER_DIV8, 0xFF);   // Set the frequency of the timer.
                                                            //  Duty is fixed at 50%.
    attachInterrupt(emergency_button_pin, emergency_button_handler, CHANGE);
    clock.begin();
    clock.setDate(1, 1, 16);
    clock.setTime(0, 0, 0);
    clock.setAlarmSeconds(30);
    clock.enableAlarm(clock.MATCH_SS);
    clock.attachInterrupt(log_battery_data);

        // Do any enabling after all setup work is done
    enable_watch_timer();
}

void loop(){
        // Track if the SD card is connected or disconnected
    if(digitalRead(sd_det_pin) & bit_flags.sd_detect){
            // SD card was disconnected
        bit_flags.sd_detect = false;
        bit_flags.sd_rdy = false;
    } else if(!digitalRead(sd_det_pin) & !bit_flags.sd_detect){
            // A new SD card was connected
        bit_flags.sd_detect = true;
        init_sd();
    }

        // Deal with any pending requests by the timer ISR
    if(bit_flags.log_block){
        add_log_block();
        bit_flags.log_block = false;
    }
    if(bit_flags.initiate_startup){
        attempt_startup();
        bit_flags.initiate_startup = false;
    }
    while (SerialUSB.available()) // If data is sent to the monitor
    {
        digitalWrite(PIN_LED_RXL, LOW); // Borrow RX led to acknowledge receipt of data
        buf_sz = SerialUSB.readBytes(ser_buf, BUFFER_MAX_SIZE);
        digitalWrite(PIN_LED_RXL, HIGH);
        for(size_type i = 0; i < buf_sz; ++i){
            byte com = ser_buf[i];  // Save multiple dereferences
                                    //  from occurring
            switch(com & 0x7){  // Extract the bits reserved for commands
                case 0x2:
                    shutdown_immediate();
                    break;
                case 0x3:
                    shutdown_gradual(com >> 3);
                    break;
                case 0x4:
                        // Extract the bits reserved for
                        //  converter selection and delay.
                    Xable_converter((com >> 6), (com >> 3) & 0x7, DISABLE_LOGIC);
                    break;
                case 0x5:
                        // Extract the bits reserved for
                        //  converter selection and delay.
                    Xable_converter((com >> 6), (com >> 3) & 0x7, ENABLE_LOGIC);
                    break;
                case 0x6:
                        // Extract the bits reserved for information type
                    transmit_data(com >> 4);
                    break;
                case 0x7:   // Reset capacity
                    capacity_left = MAX_CAPACITY;
                    break;
                default:
                    break;
            }
        }
    }
}




/***************************************************************************
                            UART I/O start
***************************************************************************/
void transmit_data(byte info_type){
    digitalWrite(PIN_LED_TXL, LOW); // Visually indicate that data is sent
    switch(info_type){
    // Transmitting informational data
        case 1:
            SerialUSB.print("t");
            SerialUSB.println(static_cast<word>(battery.time));
            break;
        case 2:
            SerialUSB.print("S");
            SerialUSB.println(static_cast<byte>(battery.SoC));
            break;
        case 3:
            SerialUSB.print("C");
            SerialUSB.println(static_cast<dword>(battery.capacity));
            break;
        case 4:
            SerialUSB.print("V");
            SerialUSB.println(ADC_TO_VOLTAGE(static_cast<word>(battery.volt)));
            break;
        case 5:
            SerialUSB.print("I");
            SerialUSB.println(ADC_TO_CURRENTMA(static_cast<word>(battery.curr)));
            break;
        case 6:
            SerialUSB.print("T");
            SerialUSB.println(ADC_TO_TEMPERATURE(static_cast<word>(battery.temp)));
            break;
        case 0:
            SerialUSB.print("t");
            SerialUSB.print(static_cast<word>(battery.time));
            SerialUSB.print("S");
            SerialUSB.print(static_cast<byte>(battery.SoC));
            SerialUSB.print("C");
            SerialUSB.print(static_cast<dword>(battery.capacity));
            SerialUSB.print("V");
            SerialUSB.print(ADC_TO_VOLTAGE(static_cast<word>(battery.volt)));
            SerialUSB.print("I");
            SerialUSB.print(ADC_TO_CURRENTMA(static_cast<word>(battery.curr)));
            SerialUSB.print("T");
            SerialUSB.println(ADC_TO_TEMPERATURE(static_cast<word>(battery.temp)));
            break;
    // Transmitting emergency codes
        case 'e':
            SerialUSB.println("eC");
            break;
        case 'f':
            SerialUSB.println("eV");
            break;
        case 'g':
            SerialUSB.println("eI");
            break;
        case 'h':
            SerialUSB.println("eT");
            break;
        default: break;
    }
    digitalWrite(PIN_LED_TXL, HIGH);
}
/***************************************************************************
                            UART I/O end
***************************************************************************/



/***************************************************************************
                                Analog Start
***************************************************************************/
void measure(){
    battery.volt = analogRead(voltage_pin);
        // Measure from the 50A sensor first. If it is saturated,
        //  use the measurement from the 300A sensor. Measure both
        //  points as simultaneously as possible.
        // If using the 50A measurement, remap to the correct range.
    word high_current_meas = analogRead(current_pin);
    micro_curr = micros();
    battery.curr = analogRead(current2_pin);
    if(battery.curr > 3285 || battery.curr < 820){
        battery.curr = high_current_meas;
    } else{
        battery.curr =
            (ADC_TO_CURRENT2(battery.curr) + 300) / 600
            * ADC_VAL_RANGE
            ;
    }
    battery.temp = analogRead(temp_pin);
}

void calculate(){
        // Convert to mA and account for offset
    float current_drawn = ADC_TO_CURRENTMA(battery.curr);
    capacity_left -= 4 * current_drawn * ((micro_curr-micro_ref)/3.6e9f); // Convert to mAh
    micro_ref = micro_curr;
    if(capacity_left > MAX_CAPACITY){
        capacity_left = MAX_CAPACITY;
        battery.capacity = capacity_left;
        battery.event = OVERVOLT;
    } else if(capacity_left > 0){
        battery.capacity = capacity_left; // Prepare integer value for storage
    } else {
        battery.capacity = 0;
    }
    battery.SoC = 100*capacity_left/MAX_CAPACITY;

        // Check for certain conditions from least priority to most
    if(battery.SoC < MIN_SOC){
        battery.event = EMERGENCY;
        transmit_data('e');
        bit_flags.initiate_shutdown = true;
    }
    if(battery.volt < MIN_VOLTAGE){
        battery.event = EMERGENCY;
        transmit_data('f');
        bit_flags.initiate_shutdown = true;
    }
    if(battery.temp > MAX_TEMP){
        battery.event = OVERTEMP;
        transmit_data('h');
        bit_flags.initiate_shutdown = true;
    }
    if(current_drawn >= MAX_CURRENT){
        battery.event = OVERCURRENT;
        transmit_data('g');
        bit_flags.initiate_shutdown = true;
    }
}
/***************************************************************************
                                Analog End
***************************************************************************/



/***************************************************************************
                                Digital Start
***************************************************************************/
void emergency_button_handler(){
    if(digitalRead(emergency_button_pin) == HIGH && !bit_flags.initiate_shutdown){
        battery.event = NO_EVENT;
        bit_flags.initiate_startup = true;
    } else {
        bit_flags.initiate_startup = false;
        bit_flags.initiate_shutdown = true;
        battery.event = EMERGENCY;
        shutdown_immediate();
        bit_flags.initiate_shutdown = false;
    }
}

void attempt_startup(){
    delay(STARTUP_DELAY);
    digitalWrite(enable_pin[0], ENABLE_LOGIC);
/*
    for(byte i = 0; i < POWER_RAIL_COUNT; ++i){
        delay(STARTUP_DELAY);
        digitalWrite(enable_pin[i], ENABLE_LOGIC);
    }
*/
}

void shutdown_immediate(){
    battery.event = IMME_SHDN;
    for(byte i = 0; i < POWER_RAIL_COUNT; ++i){
        digitalWrite(enable_pin[i], DISABLE_LOGIC);
    }
}

void shutdown_gradual(dword delay_s){
    battery.event = GRAD_SHDN;
    for(byte i = 0; i < POWER_RAIL_COUNT; ++i){
        delay(delay_s*1000);
        digitalWrite(enable_pin[i], DISABLE_LOGIC);
    }
}

void Xable_converter(byte converter, dword delay_s, byte logic){
    delay(delay_s*1000);
    digitalWrite(enable_pin[converter], logic);
    battery.event = CONV0EN + converter;
    if(logic == DISABLE_LOGIC) battery.event += 4;
}
/***************************************************************************
                                Digital End
***************************************************************************/



/***************************************************************************
                            Logging I/O start
***************************************************************************/
void init_sd(){
    bit_flags.sd_rdy = SD.begin(sd_sel_pin) & bit_flags.sd_detect;

    if(bit_flags.sd_rdy){
            // Attempt to read the last recorded capacity value
            // Note: To be able to open a new SD card after the first
            //  initialization, "root.close()" had to be added to
            //  SDClass::begin() in SD.cpp
        File static_file = SD.open(load_file_path, FILE_READ);
        if(static_file.available()){
            char* num_buf = new char[static_file.available()+1];
            std::size_t i = 0;
            while(static_file.available()){
                num_buf[i++] = static_file.read();
            }
            num_buf[i] = '\0';
            capacity_left = std::strtod(num_buf, NULL);
            delete num_buf;
        } else {
            capacity_left = MAX_CAPACITY;
        }
    } else {
        capacity_left = MAX_CAPACITY;
    }
}

void log_battery_data(){
    if(!bit_flags.sd_rdy) return;

    bit_flags.log_in_progress = true;

        // Log static data (last known capacity)
    File log_file = SD.open(load_file_path, FILE_WRITE);
    log_file.seek(0);
    log_file.print(capacity_left);
    log_file.close();

        // Log the last measurements taken to the next block
    log_file = SD.open(log_file_path, FILE_WRITE);
        log_file.write(battery_data_buf, bdb_index+1);
        log_file.flush();
    log_file.close();

        // Empty the buffer
    std::memset(battery_data_buf, 0, bdb_index+1);
    bdb_index = 0;

    bit_flags.log_in_progress = false;
}

void add_log_block(){
    if(bit_flags.log_in_progress) return;
        // Create a rough ring buffer
    if(bdb_index >= (TOTAL_BATBUF_SIZE - DATA_BYTE_SIZE-PADDING_CHAR))
        bdb_index = 0;
    int num_written = sprintf(
        battery_data_buf + bdb_index, "[D%uM%uY%uh%um%us%u]",
        clock.getDay(), clock.getMonth(), clock.getYear(),
        clock.getHours(), clock.getMinutes(), clock.getSeconds()
        );
    bdb_index += num_written;
    std::memcpy(
        battery_data_buf + bdb_index,
        reinterpret_cast<byte*>(&battery),
        DATA_BYTE_SIZE
        );
    bdb_index += DATA_BYTE_SIZE;
    battery_data_buf[bdb_index] = '\r';
    battery_data_buf[bdb_index+1] = '\n';
    bdb_index += 2;
}
/***************************************************************************
                            Logging I/O end
***************************************************************************/



/***************************************************************************
                        Interrupt Handlers start
***************************************************************************/
void TC3_Handler(){
    if(watch_interrupted_overflowed()){
        ++ticks_passed;
        if(second_counter++ == second_period){
            measure();
            calculate();
            battery.time = 4*micros()/1e6;
//            battery.time = ticks_passed*DEL_T;  // Convert to seconds
            second_counter = 0x0;   // Reset the counter
            bit_flags.log_block = true;
        }
        clear_watch_interrupt_overflowed();
    }
    if(watch_interrupted_match()){
        clear_watch_interrupt_match();
    }
}
/***************************************************************************
                        Interrupt Handlers end
***************************************************************************/