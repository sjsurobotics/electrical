/***************************************************************************
                            Dependencies Start
***************************************************************************/
//#include <Reset.h>

#include <stdint.h> // Required to use types that enforce a specific size.
                    //  Note that the SAMD21 operates on a 32-bit architecture
#include <string.h> // Required for certain memory operations such as memset.
/***************************************************************************
                            Dependencies End
***************************************************************************/



/***************************************************************************
                    Master to Slave commands Start
***************************************************************************/
#define HANDSHAKE           0x1
#define SHUTDOWN_IMMEDIATE  0x2
#define SHUTDOWN_GRADUAL    0x3
#define DISABLE_SPECIFIC    0x4
#define DISABLE_ALL         0x5
#define REQUEST_INFORMATION 0x6
#define RESET_UC            0x9
/***************************************************************************
                    Master to Slave commands End
***************************************************************************/



/***************************************************************************
                    Slave to Master commands Start
***************************************************************************/
#define IMPENDING_SHUTDOWN_GRADUAL 'A'
/***************************************************************************
                    Slave to Master commands End
***************************************************************************/



/***************************************************************************
                            Type Aliases Start
***************************************************************************/
typedef uint8_t byte;
/***************************************************************************
                            Type Aliases End
***************************************************************************/



/***************************************************************************
                            Global variables Start
***************************************************************************/
bool host_connected = false;

#define BUFFER_MAX_SIZE 10
byte ser_buf[BUFFER_MAX_SIZE];
byte buf_sz = 0;    // Hold the actual number of bytes read
/***************************************************************************
                            Global variables End
***************************************************************************/



/***************************************************************************
                        Function prototypes Start
***************************************************************************/
bool handshake();   // State C
/***************************************************************************
                        Function prototypes End
***************************************************************************/



void setup()
{
    SerialUSB.begin(9600); // Initialize Serial Monitor USB

    while (!SerialUSB) ; // Wait for Serial monitor to open
}

void loop()
{
    if(!host_connected){
        if(!handshake()){
                // Until connection is established, hold Tx high.
            SerialUSB.print(0xFF);
        }
        return;
    }
    if (SerialUSB.available()) // If data is sent to the monitor
    {
        buf_sz = SerialUSB.readBytes(ser_buf, BUFFER_MAX_SIZE);
        for(unsigned short i = 0; i < buf_sz; ++i){
            unsigned short com = ser_buf[i] & 0xF, flags = (ser_buf[i] >> 4) & 0xF;
            if(com <= REQUEST_INFORMATION && com >= SHUTDOWN_IMMEDIATE){
                SerialUSB.print("Command: ");
                    SerialUSB.print(com);
                    SerialUSB.print(" | Flags: ");
                    SerialUSB.print(flags);
                    SerialUSB.print(" -->\t");
            }
            switch(com){
                case SHUTDOWN_IMMEDIATE:
                    SerialUSB.println("Shutting down all systems immediately!");
                    break;
                case SHUTDOWN_GRADUAL:
                    SerialUSB.println("Shutting down all systems gradually!");
                    break;
                case DISABLE_SPECIFIC:
                    SerialUSB.print("Disabling converter #");
                        SerialUSB.print(flags);
                        SerialUSB.println("!");
                    break;
                case DISABLE_ALL:
                    SerialUSB.println("Disabling all converters!!");
                    break;
                case REQUEST_INFORMATION:
                    SerialUSB.println("Requesting Information!");
                    break;
/*
                case RESET_UC:
                    SerialUSB.println("Initiating Reset!");
                    initiateReset(10);
                    break;
*/
                case HANDSHAKE:
                default:
                    break;
            }
        }
    }
}

bool handshake(){
    if(SerialUSB.available()){
        SerialUSB.readBytes(ser_buf, BUFFER_MAX_SIZE);
        for(unsigned short i = 0; i < BUFFER_MAX_SIZE; ++i){
            if((ser_buf[i] & 0xF) == HANDSHAKE){
                host_connected = true;
                SerialUSB.println("Successfully connected to host! Prepare for alien egg injection.");
                return true;
            }
        }
    }
    return false;
}