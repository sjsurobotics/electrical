/***************************************************************************

                            Power Controller Program

    Description: Track various information about the main battery
        pack of the Labradaor Mk. II. Determine the approximate capacity
        left in the battery pack and safely shut-off power converters
        when needed. Communicate with the main controller, and inform it
        of important information.
        Uses both C and C++.



    Priorities:
        1) Shut off all power converters in the case of a failure
            or another event.
            a) Allow manual shut-off via an emergency button in the
                case all power is lost, including for the
                Power Controller.
            b) While the rover is in operation, continuously provide
                a high signal to keep the power converters enabled.
        2) Communicate with the main controller.
            a) Pass on measured information if requested.
                See command format below.
            b) If commanded so, disable the power converters
                immediately.
            c) If the Power Controller takes the initiative to disable
                the power converters, inform the main controller
                if possible. Specifically, the Power Controller will
                emit "E2" to the main controller.
        3) Take temperature, current, and/or voltage measurements
            of the battery pack to calculate the approximate
            capacity left.
        4) Record measurements to an external EEPROM.



    UART communication with the main controller:

        Command format:
            Bits:    7 - 3       2 - 0
            Name: [ Options ] [ Command ]

        Available commands from main controller to Power Controller:
          Byte to send  |     Name      |        Description        |          Option Format
        -----------------------------------------------------------------------------------------------
              0x1       |   Handshake   |       Acknowledge the     |              None
                        |               |   Power Controller.       |
              0x2       |   Shdn. Im.   |       Disable all         |              None
                        |               |   converters immediately. |
              0x3       |  Shdn. Grad.  |       Disable converters  |             7 - 3
                        |               |   one by one gradually.   |       [Delay in seconds]
              0x4       |    Disable    |       Disable a specific  |     7 - 6         5 - 3
                        |               |   converter. The most     |  [Converter] [Delay in seconds]
                        |               |   significant two bits    |
                        |               |   determine which.        |
              0x5       |    Enable     |       Enable a specific   |     7 - 6         5 - 3
                        |               |   converter. The most     |  [Converter] [Delay in seconds]
                        |               |   significant two bits    |
                        |               |   determine which.        |
              0x6       |    Request    |       Requests specific   |              7 - 4
                        |               |   information. The most   | [Type of informaion (see below)]
                        |               |   significant four bits   |
                        |               |   determine which.        |

        When sending the request command, the most significant four
            bits specify which type of information the power
            controller should send back.
                      Hex # |      Name
                    ---------------------------
                       0x0  |       All
                       0x1  |   Time passed
                       0x2  | State of Charge
                       0x3  |  Capacity Left
                       0x4  |     Voltage
                       0x5  |     Current
                       0x6  |   Temperature
        

        The type of data sent back based on the high four bits sent
            with the Request command (Format - [Character][Number]):
          Hex # |      Name       | Char. | Bit size |     Unit
        --------------------------------------------------------------
           0x0  |       All       |       |    88    |
           0x1  |   Time passed   |   t   |    16    |       s
           0x2  | State of Charge |   S   |     8    |  25/64 %/bit
           0x3  |  Capacity Left  |   C   |    32    |      mAh
           0x4  |     Voltage     |   V   |    16    |  25/4 mV/bit
           0x5  |     Current     |   I   |    16    | 9375/64 mA/bit
           0x6  |   Temperature   |   T   |    16    | 125/1024 C/bit
        For example, if the main controller sends 0x36 and the
            capacity left is 15000 mAh, the Power Controller will respond
            with "C0005DC" (0005DC is in hexadecimal).

        Special notes:
            - The Bit size column in the table directly above does not
                include the bits for the character.
            - Information about Capacity is in reality only 20 bits
            - Information about Voltage, Current, and
                Temperature are in reality only 12 bits long.
            - 0x0 will request all information, as if the commands
                0x16, 0x26, 0x36, 0x46, 0x56, and 0x66 were sent
                consecutively.
            - 0x3 will delay the shutdown of each converter by the
                same delay one at a time. The main controller will be
                responsible if there is a need to delay only once
                before shutting down all converters at the same time.



    I2C communication with a Microchip 24FC1026 (1 Mibit) EEPROM:

        EEPROM record format in order (88 bits in total):
            - 16 bits: Time passed in startup [seconds]
            - 8 bits: State of Charge [25/64 %/bit]
            - 20 bits: Remaining Capacity [mAh]
            - 12 bits: Pack Voltage [25/4 mV/bit]
            - 12 bits: Current [9375/64 mA/bit]
            - 12 bits: Temperature [125/1024 C/bit]
            - 8 bits: Event code
                * 0x0 - Nothing
                * 0x1 - Immediate Shutdown
                * 0x2 - Gradual Shutdown
                * 0x3 - Overcurrent detected
                * 0x4 - Under/Overvoltage detected
                * 0x5 - Over-temperature detected
                * 0x6 - Emergency button pressed
                * 0x7 - Low capacity
                * 0x8 - Converter 0 enabled
                * 0x9 - Converter 1 enabled
                * 0xA - Converter 2 enabled
                * 0xB - Converter 3 enabled
                * 0xC - Converter 0 disabled
                * 0xD - Converter 1 disabled
                * 0xE - Converter 2 disabled
                * 0xF - Converter 3 disabled

***************************************************************************/







/***************************************************************************
                            Dependencies Start
***************************************************************************/
#include "EEPROM_24FC1026.h"    // Required for I2C communication with the EEPROM.
#include "watch_timer.h"        // Required for setting up timed interrupts
#include "byte_types.h"         // Required for types such as word and size_type
/***************************************************************************
                            Dependencies End
***************************************************************************/



/***************************************************************************
                            Type Aliases Start
***************************************************************************/

    // Commands the main controller may send to the Power Controller
typedef byte master_command;
#define HANDSHAKE           0x1
#define SHUTDOWN_IMMEDIATE  0x2
#define SHUTDOWN_GRADUAL    0x3
#define DISABLE             0x4
#define ENABLE              0x5
#define REQUEST             0x6
#define RESET_UC            0xF

    // Event codes
typedef byte event_code;
#define NO_EVENT    0x0
#define IMME_SHDN   0x1
#define GRAD_SHDN   0x2
#define OVERCURRENT 0x3
#define OVERVOLT    0x4
#define OVERTEMP    0x5
#define EMERGENCY   0x6
#define LOW_CAP     0x7
#define CONV0EN     0x8
#define CONV1EN     0x9
#define CONV2EN     0xA
#define CONV3EN     0xB
#define CONV0DIS    0xC
#define CONV1DIS    0xD
#define CONV2DIS    0xE
#define CONV3DIS    0xF

    // State enumerations
enum program_state : byte {
    A,  // Take analog measurements
    B,  // Enable Power converters
    C,  // Check if the main controller acknowledges the Power Controller
    D,  // Record data to the EEPROM and reset any event flags
    E,  // Receive/Transmit data with the main controller. Execute any
        //  commands the main controller has sent.
    F,  // Shutdown all converters immediately.
    G,  // Shutdown all converters gradually.
    H   // Perform calculations based on analog readings.
};

    // Compact POD structure for measurement information
struct Battery_Data {
    dword
        time : 16,
        SoC : 8,
        capacity : 20,  // Note that by using dword, 1 byte will be
                        //  added as padding before capacity.
        volt : 12,
        curr : 12,
        temp : 12,
        event : 8
        ;
};
/***************************************************************************
                            Type Aliases End
***************************************************************************/


/***************************************************************************
                                Macros start
***************************************************************************/
#define DATA_BYTE_SIZE sizeof(Battery_Data)

    // Maximum and minimum ratings
#define MAX_CAPACITY 19600  // The maximum capacity of the battery pack (mAh)
#define MIN_CAPACITY 4900   // The minimum capacity allowable before the
                            //  Power Controller begins shutting down
                            //  converters (mAh). This is
                            //  defined as the approximate capacity left
                            //  when the battery pack has reached its
                            //  lowest allowable voltage.
#define MAX_CURRENT 250e3
#define MIN_VOLTAGE 3839    // Minimum as if read by ADC (24 V)
#define MAX_TEMP 1310       // Maximum as if read by ADC (100 C)

#define DEL_T 62.5f // The change in time between analog readings (micro-seconds)
#define ADC_VAL_RANGE 4096

#define ADC_TO_CURRENT(WORD) (WORD * 600/4095 - 300)
/***************************************************************************
                                Macros end
***************************************************************************/



/***************************************************************************
                            Global variables Start
***************************************************************************/
    // Digital Enable pins
const byte enable_pin[4] = {2, 3, 4, 5};
    // Digital input pin for the emergency button
const byte emergency_button_pin = 7;
    // Analog input pins
const byte
    current_pin = 3,    // Error: 12 (raw)  --> 1.76 A
    temp_pin = 2        // Error: 13 (raw)  --> 1.05 C
    ;

    // EEPROM and measurement-related variables
EEPROM::Address_Select data_address;
Battery_Data battery;
event_code last_event;
float capacity_left = MAX_CAPACITY; // Store the capacity in a separate
                                    //  entity that can keep the precision
const size_type second_period = 0x5;
size_type second_counter = 0;       // Upon matching second_period, trigger
                                    //  logging to EEPROM.

    // Program state-related variables
program_state current_state;

    // UART-related variables
bool host_connected = false;
#define BUFFER_MAX_SIZE 10
byte ser_buf[BUFFER_MAX_SIZE];
byte buf_sz = 0;    // Hold the actual number of bytes read
/***************************************************************************
                            Global variables End
***************************************************************************/



/***************************************************************************
                        Function prototypes Start
    * Address_Select objects are passed by copy since they are small PODs and
        are likely to be similar to the size of a pointer.
***************************************************************************/
    // UART I/O
bool attempt_handshake();   // State C

    // Analog measurements
void measure(); // Update the battery object with analog measurements. (State A)
void calculate();   // State H

    // Digital
void shutdown_all_immediate();      // Disable all power converters.
void shutdown_all_gradual(big_size_type delay_ms);  // After a given delay_ms,
                                                    //  disable a converter.
void shutdown_gradual(      // Disable a specific converter
                            //  after a given delay_ms.
    size_type converter,
    big_size_type delay_ms
    );

    // EEPROM I/O start
void log_battery_data();    // Address is updated to point to the
                            //  next byte after the newly written
                            //  information.
                            //  Write the entire structure to the
                            //  EEPROM. Since this is a POD, this
                            //  should be as close as possible to
                            //  the individual members.
#define DEBUG_MISC
#ifdef DEBUG_MISC
    void read_last_log_entry();
#endif

/***************************************************************************
                        Function prototypes End
***************************************************************************/


void setup(){
    EEPROM::init(0x0);
    SerialUSB.begin(9600);  // Initialize SerialUSB Monitor USB

        // Zero out EEPROM address and battery data
    data_address.block.segment = 0;
    data_address.block.data = 0;
    battery.time = 0;
    battery.SoC = 0xFF;
        // Set the initial battery capacity
    battery.capacity = MAX_CAPACITY;
    battery.volt = 4095;
    battery.curr = 0;
    battery.temp = 0;
    battery.event = 0x0;

        // Set up digital pins and ADC
    for(size_type i = 0; i < 4; ++i){
        pinMode(enable_pin[i], OUTPUT);
        digitalWrite(enable_pin[i], HIGH);  // Enable
    }
    pinMode(emergency_button_pin, INPUT);
    analogReadResolution(12); // Set up resolution for all analog measurements

        // Set up interrupt-related functions
    configure_watch_timer(TC_CTRLA_PRESCALER_DIV64, 0xFF);
    attachInterrupt(emergency_button_pin, shutdown_all_immediate, RISING);

        // Do any enabling after all setup work is done
    enable_watch_timer();
}

void loop(){
    while (SerialUSB.available()) // If data is sent to the monitor
    {
        buf_sz = SerialUSB.readBytes(ser_buf, BUFFER_MAX_SIZE);
        for(size_type i = 0; i < buf_sz; ++i){
            switch(ser_buf[i]){
                case '2':
                    battery.event = IMME_SHDN;
                    shutdown_all_immediate();
                    break;
                case '3':
                    battery.event = GRAD_SHDN;
                    SerialUSB.print("E2");
                    shutdown_all_gradual(1000);
                    break;
                case '4':
                    battery.event = GRAD_SHDN;
                    SerialUSB.print("E2");
                    shutdown_gradual(0, 1000);
                    SerialUSB.print("E2");
                    shutdown_gradual(1, 1000);
                    SerialUSB.print("E2");
                    shutdown_gradual(2, 1000);
                    SerialUSB.print("E2");
                    shutdown_gradual(3, 1000);
                    break;
                case '5':
                    if(digitalRead(emergency_button_pin) == LOW){
                        battery.event = CONV0EN;
                        digitalWrite(enable_pin[0], HIGH);
                        battery.event = CONV1EN;
                        digitalWrite(enable_pin[1], HIGH);
                        battery.event = CONV2EN;
                        digitalWrite(enable_pin[2], HIGH);
                        battery.event = CONV3EN;
                        digitalWrite(enable_pin[3], HIGH);
                    }
                    break;
                case 't':
                    SerialUSB.print("t");
                    SerialUSB.print(static_cast<word>(battery.time));
                    break;
                case 'S':
                    SerialUSB.print("S");
                    SerialUSB.print(static_cast<byte>(battery.SoC));
                    break;
                case 'C':
                    SerialUSB.print("C");
                    SerialUSB.print(static_cast<dword>(battery.capacity));
                    break;
                case 'V':
                    SerialUSB.print("V");
                    SerialUSB.print(static_cast<word>(battery.volt));
                    break;
                case 'I':
                    SerialUSB.print("I");
                    SerialUSB.print(static_cast<word>(battery.curr));
                    break;
                case 'T':
                    SerialUSB.print("T");
                    SerialUSB.print(static_cast<word>(battery.temp));
                    break;
                case 'A':
                    SerialUSB.print("V");
                    SerialUSB.print(static_cast<word>(battery.volt));
                    SerialUSB.print("I");
                    SerialUSB.print(static_cast<word>(battery.curr));
                    break;
#ifdef DEBUG_MISC
                case '~':
                    read_last_log_entry();
                    break;
#endif
                default:
                    SerialUSB.println("Invalid command!");
                    break;
            }
        }
    }
}




/***************************************************************************
                            UART I/O start
***************************************************************************/
bool attempt_handshake(){
    if(SerialUSB.available()){
        SerialUSB.readBytes(ser_buf, BUFFER_MAX_SIZE);
        for(size_type i = 0; i < BUFFER_MAX_SIZE; ++i){
            if((ser_buf[i] & 0xF) == HANDSHAKE){
                host_connected = true;
                return true;
            }
        }
    }
    return false;
}
/***************************************************************************
                            UART I/O end
***************************************************************************/



/***************************************************************************
                                Analog Start
***************************************************************************/
void measure(){
    battery.curr = analogRead(current_pin);
    battery.temp = analogRead(temp_pin);
}

void calculate(){
        // Convert to mA and account for offset
    float current_drawn = ADC_TO_CURRENT(battery.curr);
    capacity_left -= current_drawn * (DEL_T/1e6/3600.0f)   // Convert to mAh
        ;
    battery.capacity = capacity_left; // Prepare integer value for storage

        // Check for certain conditions from least priority to most
    if(capacity_left < MIN_CAPACITY){
        battery.event = EMERGENCY;
    }
    if(battery.volt < MIN_VOLTAGE){
        battery.event = OVERVOLT;
    }
    if(battery.temp > MAX_TEMP){
        battery.event = OVERTEMP;
    }
    if(current_drawn >= MAX_CURRENT){
        battery.event = OVERCURRENT;
    }

    battery.SoC = capacity_left/MAX_CAPACITY;
}
/***************************************************************************
                                Analog End
***************************************************************************/



/***************************************************************************
                                Digital Start
***************************************************************************/

void shutdown_all_immediate(){
    battery.event = EMERGENCY;
        // Immediately disable converters.
    for(size_type i = 0; i < 4; ++i){
        digitalWrite(enable_pin[i], LOW);
    }
}

void shutdown_all_gradual(big_size_type delay_ms){
    delay(delay_ms);
    shutdown_all_immediate();
}

void shutdown_gradual(size_type converter, big_size_type delay_ms){
    delay(delay_ms);
    digitalWrite(enable_pin[converter], LOW);
}
/***************************************************************************
                                Digital End
***************************************************************************/



/***************************************************************************
                            EEPROM I/O start
***************************************************************************/
void log_battery_data(){
    EEPROM::write(data_address, reinterpret_cast<byte*>(&battery), DATA_BYTE_SIZE);
    data_address.block.data += DATA_BYTE_SIZE;
}

#ifdef DEBUG_MISC
    void read_last_log_entry(){
        EEPROM::Address_Select a_cpy = data_address;
        a_cpy.block.data -= DATA_BYTE_SIZE;
        Battery_Data dest;
        EEPROM::read(a_cpy, reinterpret_cast<byte*>(&dest), DATA_BYTE_SIZE);
        SerialUSB.print("Battery data logged at address [");
            SerialUSB.print(a_cpy.block.segment);
            SerialUSB.print(' ');
            SerialUSB.print(a_cpy.block.data);
            SerialUSB.println("]:");
            SerialUSB.print("\tTime: ");
            SerialUSB.println(dest.time);
            SerialUSB.print("\tState of Charge: ");
            SerialUSB.println(dest.SoC);
            SerialUSB.print("\tCapacity: ");
            SerialUSB.println(dest.capacity);
            SerialUSB.print("\tVoltage: ");
            SerialUSB.println(dest.volt);
            SerialUSB.print("\tCurrent: ");
            SerialUSB.println(dest.curr);
            SerialUSB.print("\tTemperature: ");
            SerialUSB.println(dest.temp);
            SerialUSB.print("\tEvent: ");
            SerialUSB.println(dest.event);
    }
#endif
/***************************************************************************
                            EEPROM I/O end
***************************************************************************/



/***************************************************************************
                        Interrupt Handlers start
***************************************************************************/
void TC3_Handler(){
    if(watch_interrupted_overflowed()){
        measure();
        if(second_counter++ == second_period){
            log_battery_data();
            ++battery.time;
            second_counter = 0x0;   // Reset the counter
        }
        clear_watch_interrupt_overflowed();
    }
    if(watch_interrupted_match()){
        clear_watch_interrupt_match();
    }
}
/***************************************************************************
                        Interrupt Handlers end
***************************************************************************/