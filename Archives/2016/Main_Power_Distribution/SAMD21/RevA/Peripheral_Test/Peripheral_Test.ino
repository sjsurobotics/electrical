/*******************************************************************************

                                Connection test

        Used to verify proper connection to digital and analog peripherals
    for the Power Controller Board.

********************************************************************************/



/***************************************************************************
                            Global variables Start
***************************************************************************/
    // Digital Enable pins
const byte enable_pin[4] = {2, 3, 4, 5};
    // Digital input pin for the emergency button
const byte emergency_button_pin = 7;
    // Analog input pins
const byte
    current_pin = 3,    // Error: 12 (raw)  --> 1.76 A
    temp_pin = 2        // Error: 13 (raw)  --> 1.05 C
    ;

uint32_t led_counter = 0;

const byte dac_pin = A0;
uint16_t dac_counter = 0;
float
    avg_err_curr_raw = 0, last_curr = 0, max_err_curr_raw = 0, max_err_curr = 0,
    avg_err_temp_raw = 0, last_temp = 0, max_err_temp_raw = 0, max_err_temp = 0
    ;

/***************************************************************************
                            Global variables End
***************************************************************************/



/***************************************************************************
                        Mapping functions start
***************************************************************************/
#define ADC_TO_CURRENT(WORD) (WORD * 600/4095 - 300)
#define ADC_TO_TEMPERATURE(WORD) (((WORD*3.3/4096) - 0.6)/0.01)
#define ADC_TO_CURRENT_SCALE(WORD) (WORD * 600/4095)
#define ADC_TO_TEMPERATURE_SCALE(WORD) ((WORD*3.3/4096)/0.01)
/***************************************************************************
                        Mapping functions end
***************************************************************************/



void setup(void){
    analogReadResolution(12);

    pinMode(emergency_button_pin, INPUT);
    for(unsigned short i = 0; i < 4; ++i){
        pinMode(enable_pin[i], OUTPUT);
        digitalWrite(enable_pin[i], HIGH);
    }

    SerialUSB.begin(9600);

    analogWriteResolution(10);
}

void loop(void){
    bool emergency_pressed = digitalRead(emergency_button_pin);
    if(!emergency_pressed){
        for(unsigned short i = 0; i < 4; ++i)
            digitalWrite(enable_pin[i], (led_counter%4u) == i);
        float hold = analogRead(current_pin);
        last_curr = hold;
        SerialUSB.print("Voltage at A3: ");
            SerialUSB.print(hold);
            SerialUSB.print("\t\"Current\" is: ");
        hold = ADC_TO_CURRENT(hold);
            SerialUSB.print(hold);
            SerialUSB.println(" A");
            SerialUSB.print("Voltage at A2: ");
        last_temp = hold = analogRead(temp_pin);
            SerialUSB.print(hold);
            SerialUSB.print("\t\"Temperature\" is: ");
        hold = ADC_TO_TEMPERATURE(hold);
            SerialUSB.print(hold);
            SerialUSB.println(" degrees C");

        max_err_curr_raw = max_err_temp_raw = 0.0;
    } else if(led_counter > 0) {
        float curr = analogRead(current_pin);
        float temp = analogRead(temp_pin);
        float err_curr = (curr-last_curr);
        float err_temp = (temp-last_temp);
        avg_err_curr_raw = (err_curr + avg_err_curr_raw)/2.0f;
        avg_err_temp_raw = (err_temp + avg_err_temp_raw)/2.0f;
        last_curr = curr;
        last_temp = temp;
        if(err_curr > max_err_curr_raw) max_err_curr_raw = err_curr;
        if(err_temp > max_err_temp_raw) max_err_temp_raw = err_temp;

        SerialUSB.print("Average Current error: ");
            SerialUSB.print(avg_err_curr_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_CURRENT_SCALE(avg_err_curr_raw));
            SerialUSB.println(" A");
            SerialUSB.print("\tMax error: ");
            SerialUSB.print(max_err_curr_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_CURRENT_SCALE(max_err_curr_raw));
            SerialUSB.println(" A");
            SerialUSB.print("Average Temperature error: ");
            SerialUSB.print(avg_err_temp_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_TEMPERATURE_SCALE(avg_err_temp_raw));
            SerialUSB.println(" degrees C");
            SerialUSB.print("\tMax error: ");
            SerialUSB.print(max_err_temp_raw);
            SerialUSB.print(" (raw)\t--> ");
            SerialUSB.print(ADC_TO_TEMPERATURE_SCALE(max_err_temp_raw));
            SerialUSB.println(" degrees C");
    } else {
        last_curr = analogRead(current_pin);
        last_temp = analogRead(temp_pin);
    }

    analogWrite(dac_pin, dac_counter);

    ++led_counter;
    dac_counter += 10;
    if(dac_counter >= 1024) dac_counter = 0;
    delay(50);
}