
#include "watch_timer.h"
 
int pin_ovf_led = 13;  // debug pin for overflow led 
int pin_mc0_led = 5;  // debug pin for compare led 
unsigned int loop_count = 0;
unsigned int irq_ovf_count = 0;

int pretty_led0 = 7;
int pretty_led1 = 6;
int pretty_led2 = 4;
unsigned char led_counter = 0;

void setup() {
    SerialUSB.begin(9600);

    pinMode(pin_ovf_led, OUTPUT);   // for debug leds
    digitalWrite(pin_ovf_led, LOW); // for debug leds
    pinMode(pin_mc0_led, OUTPUT);   // for debug leds
    digitalWrite(pin_mc0_led, LOW); // for debug leds

    pinMode(pretty_led0, OUTPUT);
    digitalWrite(pretty_led0, LOW);
    pinMode(pretty_led1, OUTPUT);
    digitalWrite(pretty_led1, LOW);
    pinMode(pretty_led2, OUTPUT);
    digitalWrite(pretty_led2, LOW);

    configure_watch_timer(TC_CTRLA_PRESCALER_DIV64, 0xFF);
    enable_watch_timer();
}

void loop() {
  // dummy
  delay(0);
}

void TC3_Handler()
{
    if (watch_interrupted_overflowed()) {  // A overflow caused the interrupt
        digitalWrite(pin_ovf_led, irq_ovf_count % 2); // for debug leds
        digitalWrite(pin_mc0_led, HIGH); // for debug leds
        clear_watch_interrupt_overflowed();
        irq_ovf_count++;                 // for debug leds
        SerialUSB.println("Overflow detected!");
    }

    if (watch_interrupted_match()) {  // A compare to cc0 caused the interrupt
        digitalWrite(pin_mc0_led, LOW);  // for debug leds
        clear_watch_interrupt_match();
        ++led_counter;
        digitalWrite(pretty_led0, (led_counter | (led_counter >> 6)) & 0x1);
        digitalWrite(pretty_led1, ((led_counter >> 1) | (led_counter >> 5) | (led_counter >> 4)) & 0x1);
        digitalWrite(pretty_led2, ((led_counter >> 2) | (led_counter >> 4)) & 0x1);
        SerialUSB.println("Match detected!");
    }
}