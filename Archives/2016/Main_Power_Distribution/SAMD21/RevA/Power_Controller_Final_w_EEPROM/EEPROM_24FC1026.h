/***************************************************************************

             I2C I/O interface to the Microchip 24FC1026 EEPROM

    Provide a simple interface to read and write to the EEPROM.
    As of right now, this interface supports only one EEPROM
        with both address pins (A0 and A1) grounded.

    Limited C++ - based (Mainly because of dependency on Wire.h)

    Requirements:
        - Call EEPROM::init() in the setup() function.
        - After calling EEPROM::write(), the eeprom will
            not acknowledge until it has completed its
            writing operation. Therefore, use EEPROM::busy()
            before attempting to write to the EEPROM again.

***************************************************************************/



#ifndef MICROCHIP_EE_PR223OM_HEADRE_IO_233277F723UN32889TIONS______
#define MICROCHIP_EE_PR223OM_HEADRE_IO_233277F723UN32889TIONS______

#include "byte_types.h"



/***************************************************************************
                                Macros start
***************************************************************************/
#define M24FC1026_SIZE (1 << 17u)       // The maximum number of bytes
                                        //  the EEPROM can store.
#define M24FC1026_SEG_SIZE (1 << 16u)   // The maximum number of bytes
                                        //  a segment in the EEPROM
                                        //  can store.
#define PAGE_BYTE_SIZE 128              // The maximum number of bytes that
                                        //  may be written or read at a time.
#define ADDRESS_ATTEMPTS 5  // The maximum number of times the controller
                            //  should attempt to address an I2C device if
                            //  the device does not acknowledge.
/***************************************************************************
                                  Macros end
***************************************************************************/


namespace EEPROM{
        // Structure to organize EEPROM address selection
    typedef union {
        struct {
        // The memory space of Microchip's 24FC1026 is 1 MiB (2^17 bytes).
        //  Therefore use 17 bits for addressing each byte.
            byte
                : 7,        // Unused space
                seg : 1,    // Gets OR'ed into the control byte (after address).
                            //  Even though the datasheet labels this as B0, "B0"
                            //  is already used as a macro in Arduino.h on line 39.
                A15 : 1, A14 : 1, A13 : 1, A12 : 1, A11 : 1, A10 : 1, A9 : 1, A8 : 1,
                A7 : 1,  A6 : 1,  A5 : 1,  A4 : 1,  A3 : 1,  A2 : 1,  A1 : 1, A0 : 1
                ;
        } bit;
        struct {
            byte segment;    // B0, or seg
            word data;       // A15 - A0
        } block;
    } Address_Select;



        // Address_Select objects are passed by copy since they
        //  are small PODs and are likely to be similar to the
        //  size of a pointer.
    void init(byte address_select_bits); // Pass the two bits A1 and A0, e.g. if
                                         //    A1 is HIGH and A0 is LOW, use
                                         //    EEPROM::init(0x2);
    bool busy(); // Check if the EEPROM has finished any writing operations
    bool write(Address_Select addr, byte data_to_write);
    bool write(Address_Select addr, byte src[], size_type sz);      // Page write up to 128 bytes
    byte read(Address_Select addr);
    size_type read(Address_Select addr, byte dest[], size_type sz); // Page read up to 128 bytes
}

#endif