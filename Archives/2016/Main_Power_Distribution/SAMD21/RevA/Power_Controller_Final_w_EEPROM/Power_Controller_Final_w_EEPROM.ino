/***************************************************************************

                            Power Controller Program

    Description: Track various information about the main battery
        pack of the Labradaor Mk. II. Determine the approximate capacity
        left in the battery pack and safely shut-off power converters
        when needed. Communicate with the main controller, and inform it
        of important information.
        Uses both C and C++.



    Priorities:
        1) Shut off all power converters in the case of a failure
            or another event.
            a) Allow manual shut-off via an emergency button in the
                case all power is lost, including for the
                Power Controller.
            b) While the rover is in operation, continuously provide
                a high signal to keep the power converters enabled.
        2) Communicate with the main controller.
            a) Pass on measured information if requested.
                See command format below.
            b) If commanded so, disable the power converters
                immediately.
            c) If the Power Controller takes the initiative to disable
                the power converters, inform the main controller
                if possible. Specifically, the Power Controller will
                emit "E2" for every converter before it is
                gradually disabled.
        3) Take temperature, current, and/or voltage measurements
            of the battery pack to calculate the approximate
            capacity left.
        4) Record measurements to an external EEPROM.



    UART communication with the main controller:

        Command format:
            Bits:    7 - 3       2 - 0
            Name: [ Options ] [ Command ]

        Available commands from main controller to Power Controller:
          Byte to send  |     Name      |        Description        |          Option Format
        -----------------------------------------------------------------------------------------------
              0x2       |   Shdn. Im.   |       Disable all         |              None
                        |               |   converters immediately. |
              0x3       |  Shdn. Grad.  |       Disable converters  |             7 - 3
                        |               |   one by one gradually.   |       [Delay in seconds]
              0x4       |    Disable    |       Disable a specific  |     7 - 6         5 - 3
                        |               |   converter. The most     |  [Converter] [Delay in seconds]
                        |               |   significant two bits    |
                        |               |   determine which.        |
              0x5       |    Enable     |       Enable a specific   |     7 - 6         5 - 3
                        |               |   converter. The most     |  [Converter] [Delay in seconds]
                        |               |   significant two bits    |
                        |               |   determine which.        |
              0x6       |    Request    |       Requests specific   |              7 - 4
                        |               |   information. The most   | [Type of informaion (see below)]
                        |               |   significant four bits   |
                        |               |   determine which.        |

        When sending the request command, the most significant four
            bits specify which type of information the power
            controller should send back.
                      Hex # |      Name
                    ---------------------------
                       0x0  |       All
                       0x1  |   Time passed
                       0x2  | State of Charge
                       0x3  |  Capacity Left
                       0x4  |     Voltage
                       0x5  |     Current
                       0x6  |   Temperature
        

        The type of data sent back based on the high four bits sent
            with the Request command (Format - [Character][Number]):
          Hex # |      Name       | Char. | Bit size |     Unit
        --------------------------------------------------------------
           0x0  |       All       |       |    88    |
           0x1  |   Time passed   |   t   |    16    |       s
           0x2  | State of Charge |   S   |     8    |  25/64 %/bit
           0x3  |  Capacity Left  |   C   |    32    |      mAh
           0x4  |     Voltage     |   V   |    16    |  25/4 mV/bit
           0x5  |     Current     |   I   |    16    | 9375/64 mA/bit
           0x6  |   Temperature   |   T   |    16    | 125/1024 C/bit
        For example, if the main controller sends 0x36 and the
            capacity left is 15000 mAh, the Power Controller will respond
            with "C0005DC" (0005DC is in hexadecimal).

        Special notes:
            - The Bit size column in the table directly above does not
                include the bits for the character.
            - Information about Capacity is in reality only 20 bits
            - Information about Voltage, Current, and
                Temperature are in reality only 12 bits long.
            - 0x0 will request all information, as if the commands
                0x16, 0x26, 0x36, 0x46, 0x56, and 0x66 were sent
                consecutively.
            - 0x3 will delay the shutdown of each converter by the
                same delay one at a time. The main controller will be
                responsible if there is a need to delay only once
                before shutting down all converters at the same time.



    Emergency messages to the main controller:

        In the event of a failure or another event which causes this
    controller to force shutdown all converters, a message may be
    transmitted to the main controller. If the shutdown is immediate,
    disabling converters will take priority over alerting the main
    controller. However, if the converters will be disabled gradually,
    i.e. with a delay, one of the following messages will be transmitted
    to the main controller:
        - "eV": Undervoltage detected
        - "eT": Over temperature detected
        - "eI": Over current detected
        - "eC": Low capacity detected
            * This low capacity is defined as the capacity when damage
                to the battery pack would occur under continued operation.
        - Note that all emergency shutdown messages can be
            identified with "e" as the starting character.




    I2C communication with a Microchip 24FC1026 (1 Mibit) EEPROM:

        EEPROM record format in order (88 bits in total):
            - 16 bits: Time passed in startup [seconds]
            - 8 bits: State of Charge [25/64 %/bit]
            - 20 bits: Remaining Capacity [mAh]
            - 12 bits: Pack Voltage [25/4 mV/bit]
            - 12 bits: Current [9375/64 mA/bit]
            - 12 bits: Temperature [125/1024 C/bit]
            - 8 bits: Event code
                * 0x0 - Nothing
                * 0x1 - Immediate Shutdown
                * 0x2 - Gradual Shutdown
                * 0x3 - Overcurrent detected
                * 0x4 - Under/Overvoltage detected
                * 0x5 - Over-temperature detected
                * 0x6 - Emergency button pressed
                * 0x7 - Low capacity
                * 0x8 - Converter 0 enabled
                * 0x9 - Converter 1 enabled
                * 0xA - Converter 2 enabled
                * 0xB - Converter 3 enabled
                * 0xC - Converter 0 disabled
                * 0xD - Converter 1 disabled
                * 0xE - Converter 2 disabled
                * 0xF - Converter 3 disabled

***************************************************************************/



/***************************************************************************
                            Start macro switches
***************************************************************************/
#define DEBUG_MISC  // Allow the transmission of debug data via UART
/***************************************************************************
                            Start macro end
***************************************************************************/






/***************************************************************************
                            Dependencies Start
***************************************************************************/
#include "EEPROM_24FC1026.h"    // Required for I2C communication with the EEPROM.
#include "watch_timer.h"        // Required for setting up timed interrupts
#include "byte_types.h"         // Required for types such as word and size_type
/***************************************************************************
                            Dependencies End
***************************************************************************/



/***************************************************************************
                            Type Aliases Start
***************************************************************************/

    // Commands the main controller may send to the Power Controller
typedef byte master_command;
#define HANDSHAKE           0x1
#define SHUTDOWN_IMMEDIATE  0x2
#define SHUTDOWN_GRADUAL    0x3
#define DISABLE             0x4
#define ENABLE              0x5
#define REQUEST             0x6
#define RESET_UC            0xF

    // Event codes
typedef byte event_code;
#define NO_EVENT    0x0
#define IMME_SHDN   0x1
#define GRAD_SHDN   0x2
#define OVERCURRENT 0x3
#define OVERVOLT    0x4
#define OVERTEMP    0x5
#define EMERGENCY   0x6
#define LOW_CAP     0x7
#define CONV0EN     0x8
#define CONV1EN     0x9
#define CONV2EN     0xA
#define CONV3EN     0xB
#define CONV0DIS    0xC
#define CONV1DIS    0xD
#define CONV2DIS    0xE
#define CONV3DIS    0xF

    // State enumerations
enum program_state : byte {
    A,  // Take analog measurements
    B,  // Enable Power converters
    C,  // Check if the main controller acknowledges the Power Controller
    D,  // Record data to the EEPROM and reset any event flags
    E,  // Receive/Transmit data with the main controller. Execute any
        //  commands the main controller has sent.
    F,  // Shutdown all converters immediately.
    G,  // Shutdown all converters gradually.
    H   // Perform calculations based on analog readings.
};

    // Compact POD structure for measurement information
struct Battery_Data {
    dword
        time : 16,
        SoC : 8,
        capacity : 20,  // Note that by using dword, 1 byte will be
                        //  added as padding before capacity.
        volt : 12,
        curr : 12,
        temp : 12,
        event : 8
        ;
};
/***************************************************************************
                            Type Aliases End
***************************************************************************/


/***************************************************************************
                                Macros start
***************************************************************************/
#define DATA_BYTE_SIZE sizeof(Battery_Data)

    // Maximum and minimum ratings
#define MAX_CAPACITY 19600  // The maximum capacity of the battery pack (mAh)
#define MIN_SOC 40          // The minimum State of Charge allowable before
                            //  the Power Controller begins shutting down
                            //  converters (%). This is defined as the
                            //  approximate capacity left when the battery
                            //  pack has reached its lowest allowable voltage.
#define MAX_CURRENT 250e3
#define MIN_VOLTAGE 3724    // Minimum as if read by ADC (24 V)
#define MAX_VOLTAGE 4034    // Maximum as if read by ADC (26 V)
#define MAX_TEMP 1310       // Maximum as if read by ADC (100 C)

#define DEL_T 940e-6f // The change in time between analog readings (seconds)
#define ADC_VAL_RANGE 4096

#define ADC_TO_VOLTAGE(WORD) (WORD * 3.3/4095)
#define CURR_ERR 0
#define ADC_TO_CURRENTMA(WORD) (1000.0f*WORD * 600/4095 - 300e3)
#define TEMP_ERR 0.035f
#define ADC_TO_TEMPERATURE(WORD) (((WORD*3.3/4096) - 0.6 -TEMP_ERR)/0.01)

    // Create macros for enable logic of converters
    //  to make switching between positive and negative logic easier.
#define ENABLE_LOGIC    HIGH
#define DISABLE_LOGIC   LOW

    // Control how fast each power system comes online
#define STARTUP_DELAY 2000
/***************************************************************************
                                Macros end
***************************************************************************/



/***************************************************************************
                            Global variables Start
***************************************************************************/
    // Digital Enable pins
const byte enable_pin[4] = {2, 3, 4, 5};
    // Digital input pin for the emergency button
const byte emergency_button_pin = 7;
    // Analog input pins
const byte
    current_pin = 3,    // Error: 12 (raw)  --> 1.76 A
    temp_pin = 2        // Error: 13 (raw)  --> 1.05 C
    ;

    // Bit flags and booleans
union {
    struct {
        byte
            start_log : 1,
            timing_state : 1,
            initiate_shutdown : 1,
            initiate_startup : 1
            ;
    };
    byte reg;
} bit_flags;
const byte timing_pin  = 13;
byte switch_button_interrupt = 0;   // 0 - Do nothing
                                    // 1 - Switch to emergency shutdown
                                    // 2 - Switch to attempt startup

    // EEPROM and measurement-related variables
EEPROM::Address_Select data_address;
Battery_Data battery;
event_code last_event = NO_EVENT;
big_size_type ticks_passed = 0;     // Store the number of ticks (each tick is
                                    //  DEL_T seconds) separately for calculations.
float capacity_left = MAX_CAPACITY; // Store the capacity in a separate
                                    //  entity that can keep the precision
const byte reserved_eeprom_bytes = 16;  // Leave room for copies of
                                        //  double word integers and their
                                        //  parity forms.
const size_type second_period = 1000;
size_type second_counter = 0;       // Upon matching second_period, trigger
                                    //  logging to EEPROM.

    // Program state-related variables
program_state current_state;

    // UART-related variables
#define BUFFER_MAX_SIZE 10
byte ser_buf[BUFFER_MAX_SIZE];
byte buf_sz = 0;    // Hold the actual number of bytes read
/***************************************************************************
                            Global variables End
***************************************************************************/



/***************************************************************************
                        Function prototypes Start
    * Address_Select objects are passed by copy since they are small PODs and
        are likely to be similar to the size of a pointer.
***************************************************************************/
    // UART I/O
void transmit_data(byte info_type);

    // Analog measurements
void measure(); // Update the battery object with analog measurements. (State A)
void calculate();   // State H

    // Digital
void emergency_button_handler();
void attempt_startup();
void shutdown_immediate();      // Disable all power converters.
void shutdown_gradual(dword delays);    // Disable all converters one at
                                        //  a time with delay delays
                                        //  in seconds
void emergency_shutdown();
    // Disable or enable a specified converter after delay seconds.
void Xable_converter(byte converter, dword delays, byte logic);

    // EEPROM I/O start
void log_battery_data();    // Address is updated to point to the
                            //  next byte after the newly written
                            //  information.
                            //  Write the entire structure to the
                            //  EEPROM. Since this is a POD, this
                            //  should be as close as possible to
                            //  the individual members.
#ifdef DEBUG_MISC
    void read_last_log_entry();
#endif

/***************************************************************************
                        Function prototypes End
***************************************************************************/


void setup(){
    EEPROM::init(0x0);
    SerialUSB.begin(115200);  // Initialize SerialUSB Monitor USB

        // Initialize the bitflags
    bit_flags.start_log = false;
    bit_flags.timing_state = false;
    bit_flags.initiate_shutdown = false;
    bit_flags.initiate_startup = false;

        // Zero out EEPROM address and battery data
    data_address.block.segment = 0;
    data_address.block.data = reserved_eeprom_bytes;    // Reserved bytes are
                                                        //  used to store last
                                                        //  State of Charge.
    battery.time = 0;
        // Set the initial battery capacity
    while(EEPROM::busy()){}
    byte initial_capacity[4*sizeof(dword)];
    EEPROM::Address_Select capacity_address;
        capacity_address.block.segment = 0;
        capacity_address.block.data = 0;
    EEPROM::read(capacity_address, initial_capacity, 4*sizeof(dword));
        // Check if capacity copies match and parity matches up
    if(
        initial_capacity[0] == initial_capacity[4] &&
        initial_capacity[1] == initial_capacity[5] &&
        initial_capacity[2] == initial_capacity[6] &&
        initial_capacity[3] == initial_capacity[7] &&
        (initial_capacity[0] ^ 0xFF) == initial_capacity[8] &&
        (initial_capacity[1] ^ 0xFF) == initial_capacity[9] &&
        (initial_capacity[2] ^ 0xFF) == initial_capacity[10] &&
        (initial_capacity[3] ^ 0xFF) == initial_capacity[11] &&
        (initial_capacity[0] ^ 0xFF) == initial_capacity[12] &&
        (initial_capacity[1] ^ 0xFF) == initial_capacity[13] &&
        (initial_capacity[2] ^ 0xFF) == initial_capacity[14] &&
        (initial_capacity[3] ^ 0xFF) == initial_capacity[15]
    ){
        capacity_left =   (initial_capacity[3] << 24u)
                        | (initial_capacity[2] << 16u)
                        | (initial_capacity[1] << 8u)
                        | initial_capacity[0];
    } else {
        capacity_left = MAX_CAPACITY;
    }
    battery.capacity = capacity_left;
    battery.SoC = 100*capacity_left/MAX_CAPACITY;
        // Set other initial values
    battery.volt = 4095;
    battery.curr = 0;
    battery.temp = 0;
    battery.event = 0x0;

        // Set up digital pins and ADC
    for(size_type i = 0; i < 4; ++i){
        pinMode(enable_pin[i], OUTPUT);
        digitalWrite(enable_pin[i], HIGH);  // Enable
    }
    pinMode(emergency_button_pin, INPUT);
    analogReadResolution(12); // Set up resolution for all analog measurements
    pinMode(timing_pin, OUTPUT);

        // Set up interrupt-related functions
    configure_watch_timer(TC_CTRLA_PRESCALER_DIV8, 0xFF);   // Set the frequency of the timer.
                                                            //  Duty is fixed at 50%.
    attachInterrupt(emergency_button_pin, emergency_button_handler, CHANGE);

        // Do any enabling after all setup work is done
    enable_watch_timer();
}

void loop(){
        // Deal with any pending requests by the timer ISR
    if(bit_flags.start_log){
        log_battery_data();
        bit_flags.start_log = false;
    }
    if(bit_flags.initiate_startup){
        attempt_startup();
        bit_flags.initiate_startup = false;
    }
    while (SerialUSB.available()) // If data is sent to the monitor
    {
        buf_sz = SerialUSB.readBytes(ser_buf, BUFFER_MAX_SIZE);
        for(size_type i = 0; i < buf_sz; ++i){
            byte com = ser_buf[i];  // Save multiple dereferences
                                    //  from occurring
            switch(com & 0x7){  // Extract the bits reserved for commands
                case 0x2:
                    shutdown_immediate();
                    break;
                case 0x3:
                    shutdown_gradual(com >> 3);
                    break;
                case 0x4:
                        // Extract the bits reserved for
                        //  converter selection and delay.
                    Xable_converter((com >> 6), (com >> 3) & 0x7, DISABLE_LOGIC);
                    break;
                case 0x5:
                        // Extract the bits reserved for
                        //  converter selection and delay.
                    Xable_converter((com >> 6), (com >> 3) & 0x7, ENABLE_LOGIC);
                    break;
                case 0x6:
                        // Extract the bits reserved for information type
                    transmit_data(com >> 4);
                    break;
#ifdef DEBUG_MISC
                case 0x8:
                    read_last_log_entry();
                    break;
#endif
                case 0x7:   // Reset capacity
                    capacity_left = MAX_CAPACITY;
                    break;
                default:
#ifdef DEBUG_MISC
                    SerialUSB.println("Invalid command!");
#endif
                    break;
            }
        }
    }
}




/***************************************************************************
                            UART I/O start
***************************************************************************/
void transmit_data(byte info_type){
    switch(info_type){
        case 1:
            SerialUSB.print("t");
            SerialUSB.println(static_cast<word>(battery.time));
            break;
        case 2:
            SerialUSB.print("S");
            SerialUSB.println(static_cast<byte>(battery.SoC));
            break;
        case 3:
            SerialUSB.print("C");
            SerialUSB.println(static_cast<dword>(battery.capacity));
            break;
        case 4:
            SerialUSB.print("V");
            SerialUSB.println(ADC_TO_VOLTAGE(static_cast<word>(battery.volt)));
            break;
        case 5:
            SerialUSB.print("I");
            SerialUSB.println(ADC_TO_CURRENTMA(static_cast<word>(battery.curr)));
            break;
        case 6:
            SerialUSB.print("T");
            SerialUSB.println(ADC_TO_TEMPERATURE(static_cast<word>(battery.temp)));
            break;
        case 0:
            SerialUSB.print("t");
            SerialUSB.println(static_cast<word>(battery.time));
            SerialUSB.print("S");
            SerialUSB.println(static_cast<byte>(battery.SoC));
            SerialUSB.print("C");
            SerialUSB.println(static_cast<dword>(battery.capacity));
            SerialUSB.print("V");
            SerialUSB.println(ADC_TO_VOLTAGE(static_cast<word>(battery.volt)));
            SerialUSB.print("I");
            SerialUSB.println(ADC_TO_CURRENTMA(static_cast<word>(battery.curr)));
            SerialUSB.print("T");
            SerialUSB.println(ADC_TO_TEMPERATURE(static_cast<word>(battery.temp)));
            break;
#ifdef DEBUG_MISC
        case 15:
            read_last_log_entry();
            break;
#endif
        default: break;
    }
}
/***************************************************************************
                            UART I/O end
***************************************************************************/



/***************************************************************************
                                Analog Start
***************************************************************************/
void measure(){
    battery.curr = analogRead(current_pin) + CURR_ERR;
    battery.temp = analogRead(temp_pin);
}

void calculate(){
        // Convert to mA and account for offset
    float current_drawn = ADC_TO_CURRENTMA(battery.curr);
    capacity_left -= current_drawn * (DEL_T/3600.0f); // Convert to mAh
    if(capacity_left >= MAX_CAPACITY){
        capacity_left = MAX_CAPACITY;
        battery.capacity = capacity_left;
        battery.event = OVERVOLT;
    } else if(capacity_left > 0){
        battery.capacity = capacity_left; // Prepare integer value for storage
    } else {
        battery.capacity = 0;
    }
    battery.SoC = 100*capacity_left/MAX_CAPACITY;

        // Check for certain conditions from least priority to most
    if(battery.SoC < MIN_SOC){
        battery.event = EMERGENCY;
        SerialUSB.println("eC");
        bit_flags.initiate_shutdown = true;
    }
    if(battery.volt < MIN_VOLTAGE){
        battery.event = EMERGENCY;
        SerialUSB.println("eV");
        bit_flags.initiate_shutdown = true;
    }
    if(battery.temp > MAX_TEMP){
        battery.event = OVERTEMP;
        SerialUSB.println("eT");
        bit_flags.initiate_shutdown = true;
    }
    if(current_drawn >= MAX_CURRENT){
        battery.event = OVERCURRENT;
        SerialUSB.println("eI");
        bit_flags.initiate_shutdown = true;
    }
}
/***************************************************************************
                                Analog End
***************************************************************************/



/***************************************************************************
                                Digital Start
***************************************************************************/
void emergency_button_handler(){
    if(digitalRead(emergency_button_pin) == HIGH && !bit_flags.initiate_shutdown){
        battery.event = NO_EVENT;
        bit_flags.initiate_startup = true;
    } else {
        bit_flags.initiate_startup = false;
        bit_flags.initiate_shutdown = true;
        battery.event = EMERGENCY;
        shutdown_immediate();
        bit_flags.initiate_shutdown = false;
    }
}

void attempt_startup(){
    delay(STARTUP_DELAY/2);
    digitalWrite(enable_pin[0], ENABLE_LOGIC);
    delay(STARTUP_DELAY);
    digitalWrite(enable_pin[1], ENABLE_LOGIC);
    delay(STARTUP_DELAY);
    digitalWrite(enable_pin[2], ENABLE_LOGIC);
    delay(STARTUP_DELAY);
    digitalWrite(enable_pin[3], ENABLE_LOGIC);
}

void shutdown_immediate(){
    battery.event = IMME_SHDN;
    digitalWrite(enable_pin[0], DISABLE_LOGIC);
    digitalWrite(enable_pin[1], DISABLE_LOGIC);
    digitalWrite(enable_pin[2], DISABLE_LOGIC);
    digitalWrite(enable_pin[3], DISABLE_LOGIC);
}

void shutdown_gradual(dword delay_s){
    battery.event = GRAD_SHDN;
    SerialUSB.print("E2");
    delay(delay_s*1000);
    digitalWrite(enable_pin[0], DISABLE_LOGIC);
    SerialUSB.print("E2");
    delay(delay_s*1000);
    digitalWrite(enable_pin[1], DISABLE_LOGIC);
    SerialUSB.print("E2");
    delay(delay_s*1000);
    digitalWrite(enable_pin[2], DISABLE_LOGIC);
    SerialUSB.print("E2");
    delay(delay_s*1000);
    digitalWrite(enable_pin[3], DISABLE_LOGIC);
}

void Xable_converter(byte converter, dword delay_s, byte logic){
    delay(delay_s*1000);
    digitalWrite(enable_pin[converter], logic);
    battery.event = CONV0EN + converter;
    if(logic == DISABLE_LOGIC) battery.event += 4;
}
/***************************************************************************
                                Digital End
***************************************************************************/



/***************************************************************************
                            EEPROM I/O start
***************************************************************************/
void log_battery_data(){
        // Log static data (last known capacity)
    static dword last_logged_capacity = 0;
    if(
        last_logged_capacity > (capacity_left+50) ||
        last_logged_capacity < (capacity_left-50)
    ){
        EEPROM::Address_Select capacity_address;
            capacity_address.block.segment = 0;
            capacity_address.block.data = 0;
        dword capacity_convert = capacity_left; // Don't depend on battery.capacity,
                                                //  which is already truncated
        byte capacity_cutup[sizeof(dword)] = { // Extract bytes
            (capacity_convert >> 24u) & 0xFF, (capacity_convert >> 16u) & 0xFF,
            (capacity_convert >> 8u) & 0xFF, capacity_convert & 0xFF,
            };
        byte towrite[16] = { // Calculate parity bits and prepare buffer for writing
            capacity_cutup[0], capacity_cutup[1],
            capacity_cutup[2], capacity_cutup[3],
            capacity_cutup[0], capacity_cutup[1],
            capacity_cutup[2], capacity_cutup[3],
            capacity_cutup[0] ^ 0xFF, capacity_cutup[1] ^ 0xFF,
            capacity_cutup[2] ^ 0xFF, capacity_cutup[3] ^ 0xFF,
            capacity_cutup[0] ^ 0xFF, capacity_cutup[1] ^ 0xFF,
            capacity_cutup[2] ^ 0xFF, capacity_cutup[3] ^ 0xFF
            };
        EEPROM::write(capacity_address, towrite, 16);
        last_logged_capacity = capacity_convert;
    }

        // Log the last measurements taken to the next block
    EEPROM::write(data_address, reinterpret_cast<byte*>(&battery), DATA_BYTE_SIZE);
    if(
        (static_cast<dword>(M24FC1026_SEG_SIZE) - data_address.block.data)
            < DATA_BYTE_SIZE
    ){
        data_address.block.segment = !data_address.block.segment;
        data_address.block.data = 0;
    } else {
        data_address.block.data += DATA_BYTE_SIZE;
    }
}

#ifdef DEBUG_MISC
    void read_last_log_entry(){
        EEPROM::Address_Select a_cpy = data_address;
        a_cpy.block.data -= DATA_BYTE_SIZE;
        Battery_Data dest;
        while(EEPROM::busy()){};    // Wait for the EEPROM to complete
                                    //  any writing operations.
        EEPROM::read(a_cpy, reinterpret_cast<byte*>(&dest), DATA_BYTE_SIZE);
        SerialUSB.print("Battery data logged at address [");
            SerialUSB.print(a_cpy.block.segment);
            SerialUSB.print(' ');
            SerialUSB.print(a_cpy.block.data);
            SerialUSB.println("]:");
            SerialUSB.print("\tTime: ");
            SerialUSB.println(dest.time);
            SerialUSB.print("\tState of Charge: ");
            SerialUSB.println(dest.SoC);
            SerialUSB.print("\tCapacity: ");
            SerialUSB.println(dest.capacity);
            SerialUSB.print("\tVoltage: ");
            SerialUSB.println(dest.volt);
            SerialUSB.print("\tCurrent: ");
            SerialUSB.println(dest.curr);
            SerialUSB.print("\tTemperature: ");
            SerialUSB.println(dest.temp);
            SerialUSB.print("\tEvent: ");
            SerialUSB.println(dest.event);
            SerialUSB.println('~');
    }
#endif
/***************************************************************************
                            EEPROM I/O end
***************************************************************************/



/***************************************************************************
                        Interrupt Handlers start
***************************************************************************/
void TC3_Handler(){
    if(watch_interrupted_overflowed()){
        measure();
        ++ticks_passed;
        calculate();
        if(second_counter++ == second_period){
        digitalWrite(timing_pin, bit_flags.timing_state = !bit_flags.timing_state);
            bit_flags.start_log = true;
            battery.time = ticks_passed*DEL_T;  // Convert to seconds
            second_counter = 0x0;   // Reset the counter
        }
        clear_watch_interrupt_overflowed();
    }
    if(watch_interrupted_match()){
        clear_watch_interrupt_match();
    }
}
/***************************************************************************
                        Interrupt Handlers end
***************************************************************************/