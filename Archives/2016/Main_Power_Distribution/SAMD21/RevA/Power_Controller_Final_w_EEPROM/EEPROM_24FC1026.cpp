#include "EEPROM_24FC1026.h"

#include <Wire.h>


/***************************************************************************
                            Global variables start
***************************************************************************/
static byte eeprom_address = 0x28;  // Note that only 6 bits are used 
                                    //  for addressing. Also note that
                                    //  the address is right shifted.

/***************************************************************************
                            Global variables end
***************************************************************************/



/***************************************************************************
                            EEPROM I/O start
***************************************************************************/
namespace EEPROM{
    void init(byte address_select_bits){
        Wire.begin();
                            // Filter out the last two bits
        eeprom_address |= address_select_bits & 0x3;
    }

    bool busy(){
        Wire.beginTransmission(eeprom_address << 1u);   // Set the 6-bit I2C address
                                                        //  and prepare a write command.
        return Wire.endTransmission();  // Immediately send STOP and check if the
                                        // EEPROM acknowledges.
    }

    bool write(Address_Select addr, byte data_to_write){
        return write(addr, &data_to_write, 1);
    }

    bool write(Address_Select addr, byte src[], size_type sz){
        if(sz > PAGE_BYTE_SIZE)    sz = PAGE_BYTE_SIZE;
        for(byte i = 0; i < ADDRESS_ATTEMPTS; ++i){
            while(busy());  // Wait for the EEPROM to complete any
                            //  writing operations.
            Wire.beginTransmission(
                (eeprom_address << 1u)  // Set the 6-bit I2C address
                | addr.bit.seg          // Select the segment as part of the memory address
                );
            Wire.write(addr.block.data >> 8u);
            Wire.write(addr.block.data);    // Top 8 bits should be truncated
            sz %= PAGE_BYTE_SIZE;
            Wire.write(src, sz);
            switch(Wire.endTransmission()){
                case 0:
                    return true;
                case 2:
                case 3:
                    break;
                case 1:
                case 4:
                default:
                    return false;
            }
        }
        return false;
    }

    byte read(Address_Select addr){
        byte toreturn = 0xFF;
        read(addr, &toreturn, 1);
        return toreturn;
    }

    size_type read(Address_Select addr, byte dest[], size_type sz){
        if(sz > PAGE_BYTE_SIZE) sz = PAGE_BYTE_SIZE;
        for(size_type i = 0; i < ADDRESS_ATTEMPTS; ++i){
            while(busy());  // Wait for the EEPROM to complete any
                            //  writing operations.
                // First, tell the EEPROM which address to read from.
            Wire.beginTransmission(
                (eeprom_address << 1u)  // Set the 6-bit I2C address
                | addr.bit.seg          // Select the segment as part of the memory address
                );
            Wire.write(addr.block.data >> 8u);
            Wire.write(addr.block.data);    // Top 8 bits should be truncated
            Wire.endTransmission(false);    // Don't send a STOP, so the EEPROM
                                            //  stops the write operation but
                                            //  updates its internal pointer to
                                            //  the sent address.

                // Now read the data.
            Wire.requestFrom(
                (eeprom_address << 1u)  // Set the 6-bit I2C address
                | addr.bit.seg,          // Select the segment as part of the memory address
                sz
                );
            if(!Wire.available()){
                continue;
            }
            byte actual_read = 0;
            while(Wire.available() && actual_read < sz){
                dest[actual_read++] = Wire.read();
            }
            return actual_read;  // Return the actual number of bytes read
        }
        return 0;
    }
}

/***************************************************************************
                               EEPROM I/O end
***************************************************************************/