/***************************************************************************
                            Dependencies Start
***************************************************************************/
#include <Wire.h>       // Required for I2C communication.

#include <stdint.h>     // Required for bit-length specific types.
/***************************************************************************
                            Dependencies End
***************************************************************************/



/***************************************************************************
                            Type Aliases Start
    * Alias only up to 32 bits since the SAMD21 runs on a 32-bit architecture.
***************************************************************************/
typedef uint8_t     byte;
typedef uint16_t    word;
typedef uint32_t    double_word;
/***************************************************************************
                            Type Aliases End
***************************************************************************/


/***************************************************************************
                                Macros start
***************************************************************************/
#define EEPROM_ADDRESS 0x28             // binary: 101000
                                        //  Note that only 6 bits are used for addressing.
                                        //  Also note that the address is right shifted.
#define EEPROM_SIZE (1 << 17u)          // The maximum number of bytes the EEPROM
                                        //  can store.
#define EEPROM_SEGMENT_SIZE (1 << 16u)  // The maximum number of bytes a segment in
                                        //  the EEPROM can store.
#define ADDRESS_ATTEMPTS 5              // The maximum number of times the controller
                                        //  should attempt to address an I2C device if
                                        //  the device does not acknowledge.
/***************************************************************************
                                Macros end
***************************************************************************/



/***************************************************************************
                            Global variables Start
***************************************************************************/
    // EEPROM-related variables
typedef union {
    struct {
    // The memory space of Microchip's 24FC1026 is 1 MiB (2^17 bytes).
    //  Therefore use 17 bits for addressing each byte.
        byte
            : 7,    // Unused space
            seg : 1,    // Gets OR'ed into the control byte (after address).
                        //  Even though the datasheet labels this as B0, "B0"
                        //  is already used as a macro in Arduino.h on line
                        //  39.
            A15 : 1, A14 : 1, A13 : 1, A12 : 1, A11 : 1, A10 : 1, A9 : 1, A8 : 1,
            A7 : 1,  A6 : 1,  A5 : 1,  A4 : 1,  A3 : 1,  A2 : 1,  A1 : 1, A0 : 1
            ;
    } bit;
    struct {
        byte segment;    // seg
        word data;       // A15 - A0
    } block;
} Address_Select;

    // UART-related variables
#define BUFFER_MAX_SIZE 10
byte ser_buf[BUFFER_MAX_SIZE];
byte buf_sz = 0;    // Hold the actual number of bytes read
/***************************************************************************
                            Global variables End
***************************************************************************/



/***************************************************************************
                        Function prototypes Start
***************************************************************************/

    // EEPROM I/O
bool write_to_eeprom(Address_Select addr, byte data_to_write);  // Pass by copy since
                                                                //  the POD is small.
word read_from_eeprom(Address_Select addr);
word read_next_from_eeprom();

    // EEPROM tests
void eeprom_write_test_single();
void eeprom_read_test_single();
void eeprom_write_test(bool incremental);
void eeprom_read_test();
/***************************************************************************
                        Function prototypes End
***************************************************************************/



void setup()
{
    Wire.begin();               // Initialize the I2C lines and join as a master
    SerialUSB.begin(9600);      // Initialize SerialUSB Monitor USB

    while (!SerialUSB) ; // Wait for SerialUSB monitor to open

    SerialUSB.println(
        "Waiting for command:"
        "\n\t1 - Write all 1's to the EEPROM."
        "\n\t2 - Write incrementally (1, 2, 3, etc.) to the EEPROM."
        "\n\t3 - Read all data from the EEPROM."
        "\n\t4 - Write a single test byte to the EEPROM."
        "\n\t5 - Read the last byte written to the EEPROM."
        );
}

void loop()
{
    if (SerialUSB.available()) // If data is sent to the monitor
    {
        buf_sz = SerialUSB.readBytes(ser_buf, BUFFER_MAX_SIZE);
        for(unsigned short i = 0; i < buf_sz; ++i){
            switch(ser_buf[i]){
                case '1':
                    eeprom_write_test(false);
                    break;
                case '2':
                    eeprom_write_test(true);
                    break;
                case '3':
                    eeprom_read_test();
                    break;
                case '4':
                    eeprom_write_test_single();
                    break;
                case '5':
                    eeprom_read_test_single();
                    break;
                default:
                    SerialUSB.println("Invalid command!");
                    break;
            }
        }
    }
}



    // EEPROM I/O

bool write_to_eeprom(Address_Select addr, byte data_to_write){
    SerialUSB.print("\n\tEntered write_to_eeprom() [ addr.block.segment = ");
        SerialUSB.print(addr.block.segment);
        SerialUSB.print(", addr.block.data = ");
        SerialUSB.print(addr.block.data);
        SerialUSB.print(", data_to_write = ");
        SerialUSB.print(data_to_write);
        SerialUSB.println(" ]");
    for(unsigned int i = 0; i < ADDRESS_ATTEMPTS; ++i){
        Wire.beginTransmission(
            (EEPROM_ADDRESS << 1u)  // Set the 6-bit I2C address
            | addr.bit.seg           // Select the segment as part of the memory address
            );
        Wire.write(addr.block.data >> 8u);
        Wire.write(addr.block.data);    // Top 8 bits should be truncated
        Wire.write(data_to_write);
        switch(Wire.endTransmission()){
            case 0:
                SerialUSB.println("Transmission successful");
                return true;
            case 2:
            case 3:
                break;
            case 1:
                SerialUSB.println("Data to transfer was too long!");
            case 4:
                SerialUSB.println("Unknown error!");
            default:
                SerialUSB.println("Unknown code recieved!");
                return false;
        }
    }
    SerialUSB.println("EEPROM never acknowledged!");
    return false;
}

word read_from_eeprom(Address_Select addr){
    SerialUSB.print("\n\tEntered read_from_eeprom() [ addr.block.segment = ");
        SerialUSB.print(addr.block.segment);
        SerialUSB.print(", addr.block.data = ");
        SerialUSB.print(addr.block.data);
        SerialUSB.println(" ]");
    for(unsigned int i = 0; i < ADDRESS_ATTEMPTS; ++i){
            // First, tell the EEPROM which address to read from.
        Wire.beginTransmission(
            (EEPROM_ADDRESS << 1u)  // Set the 6-bit I2C address
            | addr.bit.seg           // Select the segment as part of the memory address
            );
        Wire.write(addr.block.data >> 8u);
        Wire.write(addr.block.data);    // Top 8 bits should be truncated
        Wire.endTransmission(false);    // Don't send a STOP, so the EEPROM
                                        //  stops the write operation but
                                        //  updates its internal pointer to
                                        //  the sent address.

            // Now read the data.
        Wire.requestFrom(
            (EEPROM_ADDRESS << 1u)  // Set the 6-bit I2C address
            | addr.bit.seg,          // Select the segment as part of the memory address
            1
            );
        if(!Wire.available()){
            continue;
        }
        SerialUSB.println("Read successful!");
        return Wire.read();
    }
    SerialUSB.println("EEPROM never acknowledged!");
    return 0x1000;
}

word read_next_from_eeprom(){
    for(unsigned int i = 0; i < ADDRESS_ATTEMPTS; ++i){
        Wire.requestFrom(
            (EEPROM_ADDRESS << 1u), // Set the 6-bit I2C address
            1
            );
        if(!Wire.available()){
            continue;
        }
        return Wire.read();
    }
    return 0x1000;
}



    // EEPROM tests

static word counter = 0x0;

void eeprom_write_test_single(){
    SerialUSB.println("Initiating test write single.");
    --counter;
    Address_Select address;
        address.block.segment = 0;
        address.block.data = counter;

    SerialUSB.print("Writing 0xFF to segment ");
        SerialUSB.print(address.block.segment);
        SerialUSB.print(" at address ");
        SerialUSB.print(address.block.data);
    if(write_to_eeprom(address, 0xAA)){
        SerialUSB.println(" succeeded!");
    } else {
        SerialUSB.println(" failed!");
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_read_test_single(){
    SerialUSB.println("Initiating test read.");
    SerialUSB.print("Byte read: ");
    Address_Select address;
        address.block.segment = 0;
        address.block.data = counter;
        SerialUSB.println(read_from_eeprom(address));
    SerialUSB.println("\tFinished running test.");
}

void eeprom_write_test(bool incremental){
    SerialUSB.println("Initiating test write multiple.");

    Address_Select address;

    byte to_write = 0xFF;
    for(
        address.block.segment = 0;
        address.block.segment < 2;
        ++address.block.segment
    ){
        address.block.data = 0x0;
        do{
            if(incremental) ++to_write;
            SerialUSB.print("Writing ");
                SerialUSB.print(to_write);
                SerialUSB.print(" to segment ");
                SerialUSB.print(address.block.segment);
                SerialUSB.print(" at address ");
                SerialUSB.print(address.block.data);
            if(write_to_eeprom(address, to_write)){
                SerialUSB.println(" succeeded!");
            } else {
                SerialUSB.println(" failed!");
            }
        } while(--address.block.data > 0);
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_read_test(){
    SerialUSB.println("Initiating test read multiple.");

    Address_Select address;

    for(
        address.block.segment = 0;
        address.block.segment < 2;
        ++address.block.segment
    ){
        address.block.data = 0x0;
        do{
            SerialUSB.print("Byte at segment: ");
                SerialUSB.print(address.block.segment);
                SerialUSB.print(" at address ");
                SerialUSB.print(address.block.data);
                SerialUSB.print(" is ");
                SerialUSB.println(read_from_eeprom(address));
        } while(--address.block.data > 0);
    }
    SerialUSB.println("\tFinished running test.");
}