/*****************************************************************************
                            Internal EEPROM Test

        Test the size and the capability to write to the internal
    EEPROM in the SAMD21.


*****************************************************************************/
#include <cstdint>

void eeprom_emulator_init();

void setup(){
    SerialUSB.begin(9600);
    while(!SerialUSB){}
    SerialUSB.print("EEPROM inital length: ");
    SerialUSB.print(NVMCTRL->PARAM.reg);
    SerialUSB.println(" bytes.");

    eeprom_emulator_init();

    SerialUSB.print("EEPROM length afterward: ");
    SerialUSB.print(NVMCTRL->PARAM.reg);
    SerialUSB.println(" bytes.");
}

void loop(){
}

void eeprom_emulator_init(){
        // Set the location of the fuses to change EEPROM size
    std::uint_fast64_t* aux_user_row = (std::uint_fast64_t*)0x804000; // (page 30)
    *aux_user_row &= (~0x7) << 4;
}