/***************************************************************************

                                EEPROM test

    * Test the ability of the SAMD21 to communicate with Microchip's
        24FC1026 EEPROM via I2C at 3.3V logic.
    * Testing includes filling the EEPROM with arbitrary values and
        reading them back.
    * Largely C-based

***************************************************************************/





/***************************************************************************
                            Dependencies Start
***************************************************************************/
#include "EEPROM_24FC1026.h"
#include "byte_types.h"
/***************************************************************************
                            Dependencies End
***************************************************************************/



/***************************************************************************
                            Type Aliases start
***************************************************************************/
struct Battery_Data {
    dword
        time : 16,
        SoC : 8,
        capacity : 20,  // Note that by using dword, 1 byte will be
                        //  added as padding before capacity.
        volt : 12,
        curr : 12,
        temp : 12,
        event : 8
        ;
};
/***************************************************************************
                            Type Aliases end
***************************************************************************/


/***************************************************************************
                                Macros start
***************************************************************************/

#define DATA_BYTE_SIZE sizeof(Battery_Data)
/***************************************************************************
                                Macros end
***************************************************************************/



/***************************************************************************
                            Global variables Start
***************************************************************************/
    // Data-related variables
EEPROM::Address_Select fake_data_address;
Battery_Data battery;

    // UART-related variables
#define BUFFER_MAX_SIZE 10
byte ser_buf[BUFFER_MAX_SIZE];
byte buf_sz = 0;    // Hold the actual number of bytes read
/***************************************************************************
                            Global variables End
***************************************************************************/



/***************************************************************************
                        Function prototypes Start
***************************************************************************/
    // EEPROM tests
void print_com_list();
void eeprom_write_test_single();
void eeprom_read_test_single();
void eeprom_write_test(bool incremental);
void eeprom_read_test();
void eeprom_write_page_test();
void eeprom_read_page_test();
void eeprom_write_HIGH_page_test();
void eeprom_read_all_page_test();
void eeprom_write_incremental_page_test();

void eeprom_write_fake_data();
void eeprom_read_last_fake_data();
/***************************************************************************
                        Function prototypes End
***************************************************************************/



void setup()
{
    EEPROM::init(0x0);      // Initialize the I2C lines and join as a master
    SerialUSB.begin(9600);  // Initialize SerialUSB Monitor USB

    while (!SerialUSB) ; // Wait for SerialUSB monitor to open

    fake_data_address.block.segment = 0;
    fake_data_address.block.data = 0;
    battery.time = 43690;
    battery.SoC = 161;
    battery.capacity = 222453;
    battery.volt = 1521;
    battery.curr = 1838;
    battery.temp = 2328;
    battery.event = 231;

    print_com_list();
}

void loop()
{
    if (SerialUSB.available()) // If data is sent to the monitor
    {
        buf_sz = SerialUSB.readBytes(ser_buf, BUFFER_MAX_SIZE);
        for(unsigned short i = 0; i < buf_sz; ++i){
            switch(ser_buf[i]){
                case 'h':
                    print_com_list();
                    break;
                case '1':
                    eeprom_write_incremental_page_test();
                    break;
                case '2':
                    eeprom_write_test(true);
                    break;
                case '3':
                    eeprom_read_test();
                    break;
                case '4':
                    eeprom_write_test_single();
                    break;
                case '5':
                    eeprom_read_test_single();
                    break;
                case '6':
                    eeprom_write_page_test();
                    break;
                case '7':
                    eeprom_read_page_test();
                    break;
                case '8':
                    eeprom_write_HIGH_page_test();
                    break;
                case '9':
                    eeprom_read_all_page_test();
                    break;
                case '0':
                    SerialUSB.print("EEPROM is currently ");
                    if(EEPROM::busy()){
                        SerialUSB.println("busy.");
                    } else {
                        SerialUSB.println("available.");
                    }
                    break;
                case 'T':
                    eeprom_write_fake_data();
                    break;
                case 'R':
                    eeprom_read_last_fake_data();
                    break;
                default:
                    SerialUSB.println("Invalid command!");
                    break;
            }
        }
    }
}


    // EEPROM tests

static word counter = 0x0;

void print_com_list(){
    SerialUSB.print("Waiting for command [sizeof(Battery_Data) = ");
    SerialUSB.print(sizeof(Battery_Data));
    SerialUSB.println(" bytes]:"
        "\r\n\th - Print this command list."
        "\r\n\t1 - Write incrementally (1, 2, 3, etc.) to the EEPROM using page write."
        "\r\n\t2 - Write incrementally (1, 2, 3, etc.) to the EEPROM."
        "\r\n\t3 - Read all data from the EEPROM."
        "\r\n\t4 - Write a single test byte to the EEPROM."
        "\r\n\t5 - Read the last byte written to the EEPROM."
        "\r\n\t6 - Write a 16-byte page to the EEPROM."
        "\r\n\t7 - Read a 16-byte page from the EEPROM."
        "\r\n\t8 - Write all HIGH to the EEPROM."
        "\r\n\t9 - Read full contents from the EEPROM."
        "\r\n\t0 - Check if the EEPROM is ready."
        "\r\n\tT - Write fake data."
        "\r\n\tR - Read fake data."
        );
}

void eeprom_write_test_single(){
    SerialUSB.println("Initiating test write single.");
    --counter;
    EEPROM::Address_Select address;
        address.block.segment = 0;
        address.block.data = counter;
    const byte byte_to_wryte = 0xAE;

    SerialUSB.print("Writing ");
        SerialUSB.print(byte_to_wryte);
        SerialUSB.print(" to segment ");
        SerialUSB.print(address.block.segment);
        SerialUSB.print(" at address ");
        SerialUSB.print(address.block.data);
    if(EEPROM::write(address, byte_to_wryte)){
        SerialUSB.println(" succeeded!");
    } else {
        SerialUSB.println(" failed!");
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_read_test_single(){
    SerialUSB.println("Initiating test read.");
    SerialUSB.print("Byte read: ");
    EEPROM::Address_Select address;
        address.block.segment = 0;
        address.block.data = counter;
        SerialUSB.println(EEPROM::read(address));
    SerialUSB.println("\tFinished running test.");
}

void eeprom_write_test(bool incremental){
    SerialUSB.println("Initiating test write multiple.");

    EEPROM::Address_Select address;

    byte to_write = 0xFF;
    for(
        address.block.segment = 0;
        address.block.segment < 2;
        ++address.block.segment
    ){
        address.block.data = 0x0;
        do{
            if(incremental) ++to_write;
            SerialUSB.print("Writing ");
                SerialUSB.print(to_write);
                SerialUSB.print(" to segment ");
                SerialUSB.print(address.block.segment);
                SerialUSB.print(" at address ");
                SerialUSB.print(address.block.data);
            if(EEPROM::write(address, to_write)){
                SerialUSB.println(" succeeded!");
            } else {
                SerialUSB.println(" failed!");
            }
        } while(--address.block.data > 0);
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_read_test(){
    SerialUSB.println("Initiating test read multiple.");

    EEPROM::Address_Select address;

    for(
        address.block.segment = 0;
        address.block.segment < 2;
        ++address.block.segment
    ){
        address.block.data = 0x0;
        do{
            SerialUSB.print("Byte at segment: ");
                SerialUSB.print(address.block.segment);
                SerialUSB.print(" at address ");
                SerialUSB.print(address.block.data);
                SerialUSB.print(" is ");
                SerialUSB.println(EEPROM::read(address));
        } while(--address.block.data > 0);
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_write_page_test(){
    SerialUSB.println("Initiating test page write.");

    EEPROM::Address_Select address;
        address.block.segment = 0;
        address.block.data = 0;
    byte towrite[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

    SerialUSB.print("Writing 16-byte page to EEPROM ");
    if(EEPROM::write(address, towrite, 16)){
        SerialUSB.println(" succeeded!");
    } else {
        SerialUSB.println(" failed!");
    }
    
    SerialUSB.println("\tFinished running test.");
}

void eeprom_read_page_test(){
    SerialUSB.println("Initiating test page read.");

    EEPROM::Address_Select address;
        address.block.segment = 0;
        address.block.data = 0;
    byte toread[16];

    SerialUSB.print("Read ");
        SerialUSB.print(EEPROM::read(address, toread, 16));
        SerialUSB.print(" bytes from EEPROM: ");
    for(size_type i = 0; i < 16; ++i){
        SerialUSB.print(toread[i]);
        SerialUSB.print(" ");
    }
    
    SerialUSB.println("\r\n\tFinished running test.");
}

#define BUF_TEST_WRITE_SIZE 4096
#define BUF_SECTION_COUNT 16

void eeprom_write_HIGH_page_test(){
    SerialUSB.println("Initiating test write all HIGH.");

    EEPROM::Address_Select address;

    byte buf[BUF_TEST_WRITE_SIZE];
    for(unsigned i = 0; i < BUF_TEST_WRITE_SIZE; ++i){
        buf[i] = 0xFF;
    }
    for(
        address.block.segment = 0;
        address.block.segment < 2;
        ++address.block.segment
    ){
        address.block.data = 0x0;
        for(
            unsigned i = 0;
            i < BUF_SECTION_COUNT;
            ++i, address.block.data += BUF_TEST_WRITE_SIZE
        ){
            while(EEPROM::busy());   // Wait for the eeprom to finish writing operations

            SerialUSB.print("Writing ");
                SerialUSB.print(BUF_TEST_WRITE_SIZE);
                SerialUSB.print(" HIGH bytes starting on segment ");
                SerialUSB.print(address.block.segment);
                SerialUSB.print(" at address ");
                SerialUSB.print(address.block.data);
            if(EEPROM::write(address, buf, BUF_TEST_WRITE_SIZE)){
                SerialUSB.println(" succeeded!");
            } else {
                SerialUSB.println(" failed!");
            }
        }
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_read_all_page_test(){
    SerialUSB.println("Initiating test read all.");

    EEPROM::Address_Select address;
        address.block.segment = 0;
        address.block.data = 0;
    byte toread[BUF_TEST_WRITE_SIZE];

    SerialUSB.print("EEPROM contents: ");
    for(size_type i = 0; i < BUF_SECTION_COUNT; ++i){
        EEPROM::read(address, toread, BUF_TEST_WRITE_SIZE);
        for(size_type i = 0; i < BUF_TEST_WRITE_SIZE; ++i){
            SerialUSB.print(toread[i]);
            SerialUSB.print(" ");
        }
        SerialUSB.println("");
    }
    
    SerialUSB.println("\r\n\tFinished running test.");
}

void eeprom_write_incremental_page_test(){
    SerialUSB.println("Initiating test write all HIGH.");

    EEPROM::Address_Select address;

    byte buf[BUF_TEST_WRITE_SIZE];
    byte towrite = 0;
    for(unsigned i = 0; i < BUF_TEST_WRITE_SIZE; ++i, ++towrite){
        buf[i] = towrite;
    }
    for(
        address.block.segment = 0;
        address.block.segment < 2;
        ++address.block.segment
    ){
        address.block.data = 0x0;
        for(
            unsigned i = 0;
            i < BUF_SECTION_COUNT;
            ++i, address.block.data += BUF_TEST_WRITE_SIZE
        ){
            while(EEPROM::busy());   // Wait for the eeprom to finish writing operations

            SerialUSB.print("Writing ");
                SerialUSB.print(BUF_TEST_WRITE_SIZE);
                SerialUSB.print(" HIGH bytes starting on segment ");
                SerialUSB.print(address.block.segment);
                SerialUSB.print(" at address ");
                SerialUSB.print(address.block.data);
            if(EEPROM::write(address, buf, BUF_TEST_WRITE_SIZE)){
                SerialUSB.println(" succeeded!");
            } else {
                SerialUSB.println(" failed!");
            }
        }
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_write_fake_data(){
    SerialUSB.println("Battery data to write:");
        SerialUSB.print("\tTime [s]: ");
        SerialUSB.println(battery.time);
        SerialUSB.print("\tState of Charge [%/bit]: ");
        SerialUSB.println(battery.SoC);
        SerialUSB.print("\tRemaining Capacity [mA/bit]: ");
        SerialUSB.println(battery.capacity);
        SerialUSB.print("\tVoltage [mV/bit]: ");
        SerialUSB.println(battery.volt);
        SerialUSB.print("\tCurrent [mA/bit]: ");
        SerialUSB.println(battery.curr);
        SerialUSB.print("\tTemperature [C]: ");
        SerialUSB.println(battery.temp);
        SerialUSB.print("\tEvent Code: ");
        SerialUSB.println(battery.event);
        SerialUSB.print("\tReg: ");
    for(size_type i = 0; i < DATA_BYTE_SIZE; ++i){
        SerialUSB.print(*(reinterpret_cast<byte*>(&battery)+i));
        SerialUSB.print(' ');
    }
        SerialUSB.println("");
    EEPROM::write(fake_data_address, reinterpret_cast<byte*>(&battery), DATA_BYTE_SIZE);
    fake_data_address.block.data += DATA_BYTE_SIZE;
    ++battery.time;
}

void eeprom_read_last_fake_data(){
    Battery_Data hold;
    EEPROM::Address_Select addr_copy = fake_data_address;
    addr_copy.block.data -= DATA_BYTE_SIZE;
    EEPROM::read(addr_copy, reinterpret_cast<byte*>(&hold), DATA_BYTE_SIZE);
    SerialUSB.print("Battery data:\r\n");
        SerialUSB.print("\tTime [s]: ");
        SerialUSB.println(hold.time);
        SerialUSB.print("\tState of Charge [%/bit]: ");
        SerialUSB.println(hold.SoC);
        SerialUSB.print("\tRemaining Capacity [mA/bit]: ");
        SerialUSB.println(hold.capacity);
        SerialUSB.print("\tVoltage [mV/bit]: ");
        SerialUSB.println(hold.volt);
        SerialUSB.print("\tCurrent [mA/bit]: ");
        SerialUSB.println(hold.curr);
        SerialUSB.print("\tTemperature [C]: ");
        SerialUSB.println(hold.temp);
        SerialUSB.print("\tEvent Code: ");
        SerialUSB.println(hold.event);
}