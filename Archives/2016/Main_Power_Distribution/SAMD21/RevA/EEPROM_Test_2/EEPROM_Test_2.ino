/***************************************************************************

                                EEPROM test

    * Test the ability of the SAMD21 to communicate with Microchip's
        24FC1026 EEPROM via I2C at 3.3V logic.
    * Testing includes filling the EEPROM with arbitrary values and
        reading them back.
    * Largely C-based

***************************************************************************/





/***************************************************************************
                            Dependencies Start
***************************************************************************/
#include <Wire.h>       // Required for I2C communication.

#include <stdint.h>     // Required for bit-length specific types.
/***************************************************************************
                            Dependencies End
***************************************************************************/



/***************************************************************************
                            Type Aliases Start
    * Alias only up to 32 bits since the SAMD21 runs on a 32-bit architecture.
***************************************************************************/
typedef uint16_t    size_type;
typedef uint8_t     byte;
typedef uint16_t    word;
typedef uint32_t    double_word;
/***************************************************************************
                            Type Aliases End
***************************************************************************/


/***************************************************************************
                                Macros start
***************************************************************************/
#define EEPROM_ADDRESS 0x28             // binary: 101000
                                        //  Note that only 6 bits are used for addressing.
                                        //  Also note that the address is right shifted.
#define EEPROM_SIZE (1 << 17u)          // The maximum number of bytes the EEPROM
                                        //  can store.
#define EEPROM_SEGMENT_SIZE (1 << 16u)  // The maximum number of bytes a segment in
                                        //  the EEPROM can store.
#define PAGE_BYTE_SIZE 128;
#define ADDRESS_ATTEMPTS 5              // The maximum number of times the controller
                                        //  should attempt to address an I2C device if
                                        //  the device does not acknowledge.

#define DATA_BYTE_SIZE 10   // The number of bytes the data pack size is
/***************************************************************************
                                Macros end
***************************************************************************/



/***************************************************************************
                            Global variables Start
***************************************************************************/
    // EEPROM-related variables
typedef union {
    struct {
    // The memory space of Microchip's 24FC1026 is 1 MiB (2^17 bytes).
    //  Therefore use 17 bits for addressing each byte.
        byte
            : 7,        // Unused space
            seg : 1,    // Gets OR'ed into the control byte (after address).
                        //  Even though the datasheet labels this as B0, "B0"
                        //  is already used as a macro in Arduino.h on line 39.
            A15 : 1, A14 : 1, A13 : 1, A12 : 1, A11 : 1, A10 : 1, A9 : 1, A8 : 1,
            A7 : 1,  A6 : 1,  A5 : 1,  A4 : 1,  A3 : 1,  A2 : 1,  A1 : 1, A0 : 1
            ;
    } bit;
    struct {
        byte segment;    // B0, or seg
        word data;       // A15 - A0
    } block;
} Address_Select;

    // Data-related variables
Address_Select fake_data_address;
typedef union {
    struct {
        word time;
        byte SoC;
        word capacity : 12;
        word volt : 12;
        word curr : 12;
        word temp : 12;
        byte event;
    } data;
    byte reg[DATA_BYTE_SIZE];
} Battery_Data;
Battery_Data battery;

    // UART-related variables
#define BUFFER_MAX_SIZE 10
byte ser_buf[BUFFER_MAX_SIZE];
byte buf_sz = 0;    // Hold the actual number of bytes read
/***************************************************************************
                            Global variables End
***************************************************************************/



/***************************************************************************
                        Function prototypes Start
    * Address_Select objects are passed by copy since they are small PODs and
        are likely to be similar to the size of a pointer.
***************************************************************************/

    // EEPROM I/O
bool eeprom_busy(); // Check if the EEPROM has finished any writing operations
bool write_to_eeprom(Address_Select addr, byte data_to_write);
bool write_to_eeprom_str(Address_Select addr, byte src[], size_type sz);     // Page write up to 128 bytes
byte read_from_eeprom(Address_Select addr);
size_type read_from_eeprom_str(Address_Select addr, byte dest[], size_type sz);   // Page read up to 128 bytes
byte read_next_from_eeprom();

    // EEPROM tests
void print_com_list();
void eeprom_write_test_single();
void eeprom_read_test_single();
void eeprom_write_test(bool incremental);
void eeprom_read_test();
void eeprom_write_page_test();
void eeprom_read_page_test();
void eeprom_write_HIGH_page_test();
void eeprom_read_all_page_test();
void eeprom_write_incremental_page_test();

void eeprom_write_fake_data();
void eeprom_read_last_fake_data();
/***************************************************************************
                        Function prototypes End
***************************************************************************/



void setup()
{
    Wire.begin();           // Initialize the I2C lines and join as a master
    SerialUSB.begin(9600);  // Initialize SerialUSB Monitor USB

    while (!SerialUSB) ; // Wait for SerialUSB monitor to open

    fake_data_address.block.segment = 0;
    fake_data_address.block.data = 0;
    battery.data.time = 0;
    battery.data.SoC = 200;
    battery.data.capacity = 47823;
    battery.data.volt = 1000;
    battery.data.curr = 8192345;
    battery.data.temp = 34;
    battery.data.event = 0;

    print_com_list();
}

void loop()
{
    if (SerialUSB.available()) // If data is sent to the monitor
    {
        buf_sz = SerialUSB.readBytes(ser_buf, BUFFER_MAX_SIZE);
        for(unsigned short i = 0; i < buf_sz; ++i){
            switch(ser_buf[i]){
                case 'h':
                    print_com_list();
                    break;
                case '1':
                    eeprom_write_incremental_page_test();
                    break;
                case '2':
                    eeprom_write_test(true);
                    break;
                case '3':
                    eeprom_read_test();
                    break;
                case '4':
                    eeprom_write_test_single();
                    break;
                case '5':
                    eeprom_read_test_single();
                    break;
                case '6':
                    eeprom_write_page_test();
                    break;
                case '7':
                    eeprom_read_page_test();
                    break;
                case '8':
                    eeprom_write_HIGH_page_test();
                    break;
                case '9':
                    eeprom_read_all_page_test();
                    break;
                case '0':
                    SerialUSB.print("EEPROM is currently ");
                    if(eeprom_busy()){
                        SerialUSB.println("busy.");
                    } else {
                        SerialUSB.println("available.");
                    }
                    break;
                case 'T':
                    eeprom_write_fake_data();
                    break;
                case 'R':
                    eeprom_read_last_fake_data();
                    break;
                default:
                    SerialUSB.println("Invalid command!");
                    break;
            }
        }
    }
}



    // EEPROM I/O

bool eeprom_busy(){
    Wire.beginTransmission(EEPROM_ADDRESS << 1u);   // Set the 6-bit I2C address
                                                    //  and prepare a write command.
    return Wire.endTransmission();  // Immediately send STOP and check if the
                                    // EEPROM acknowledges.
}

bool write_to_eeprom(Address_Select addr, byte data_to_write){
    return write_to_eeprom_str(addr, &data_to_write, 1);
}

bool write_to_eeprom_str(Address_Select addr, byte src[], size_type sz){
    SerialUSB.print("\r\n\tEntered write_to_eeprom_str() [ addr.block.segment = ");
        SerialUSB.print(addr.block.segment);
        SerialUSB.print(", addr.block.data = ");
        SerialUSB.print(addr.block.data);
        SerialUSB.print(", sz = ");
        SerialUSB.print(sz);
        SerialUSB.println(" ]");
    for(byte i = 0; i < ADDRESS_ATTEMPTS; ++i){
        while(eeprom_busy());       // Wait for the EEPROM to complete any
                                    //  writing operations.
        Wire.beginTransmission(
            (EEPROM_ADDRESS << 1u)  // Set the 6-bit I2C address
            | addr.bit.seg          // Select the segment as part of the memory address
            );
        Wire.write(addr.block.data >> 8u);
        Wire.write(addr.block.data);    // Top 8 bits should be truncated
        sz %= PAGE_BYTE_SIZE;
        Wire.write(src, sz);
        switch(Wire.endTransmission()){
            case 0:
                SerialUSB.println("Transmission successful");
                return true;
            case 2:
            case 3:
                break;
            case 1:
                SerialUSB.println("Data to transfer was too long!");
            case 4:
                SerialUSB.println("Unknown error!");
            default:
                SerialUSB.println("Unknown code recieved!");
                return false;
        }
    }
    SerialUSB.println("EEPROM never acknowledged!");
    return false;
}

byte read_from_eeprom(Address_Select addr){
    byte toreturn = 0xFF;
    read_from_eeprom_str(addr, &toreturn, 1);
    return toreturn;
}

size_type read_from_eeprom_str(Address_Select addr, byte dest[], size_type sz){
    SerialUSB.print("\r\n\tEntered read_from_eeprom() [ addr.block.segment = ");
        SerialUSB.print(addr.block.segment);
        SerialUSB.print(", addr.block.data = ");
        SerialUSB.print(addr.block.data);
        SerialUSB.println(" ]");
    for(size_type i = 0; i < ADDRESS_ATTEMPTS; ++i){
            // First, tell the EEPROM which address to read from.
        Wire.beginTransmission(
            (EEPROM_ADDRESS << 1u)  // Set the 6-bit I2C address
            | addr.bit.seg          // Select the segment as part of the memory address
            );
        Wire.write(addr.block.data >> 8u);
        Wire.write(addr.block.data);    // Top 8 bits should be truncated
        Wire.endTransmission(false);    // Don't send a STOP, so the EEPROM
                                        //  stops the write operation but
                                        //  updates its internal pointer to
                                        //  the sent address.

            // Now read the data.
        Wire.requestFrom(
            (EEPROM_ADDRESS << 1u)  // Set the 6-bit I2C address
            | addr.bit.seg,          // Select the segment as part of the memory address
            sz
            );
        if(!Wire.available()){
            continue;
        }
        SerialUSB.println("Read successful!");
        byte actual_read = 0;
        while(Wire.available() && actual_read < sz){
            dest[actual_read++] = Wire.read();
        }
        return actual_read;  // Return the actual number of bytes read
    }
    SerialUSB.println("EEPROM never acknowledged!");
    return 0;
}

byte read_next_from_eeprom(){
    for(size_type i = 0; i < ADDRESS_ATTEMPTS; ++i){
        Wire.requestFrom(
            (EEPROM_ADDRESS << 1u), // Set the 6-bit I2C address
            1
            );
        if(!Wire.available()){
            continue;
        }
        return Wire.read();
    }
    return 0xFF;
}



    // EEPROM tests

static word counter = 0x0;

void print_com_list(){
    SerialUSB.println(
        "Waiting for command:"
        "\r\n\th - Print this command list."
        "\r\n\t1 - Write incrementally (1, 2, 3, etc.) to the EEPROM using page write."
        "\r\n\t2 - Write incrementally (1, 2, 3, etc.) to the EEPROM."
        "\r\n\t3 - Read all data from the EEPROM."
        "\r\n\t4 - Write a single test byte to the EEPROM."
        "\r\n\t5 - Read the last byte written to the EEPROM."
        "\r\n\t6 - Write a 16-byte page to the EEPROM."
        "\r\n\t7 - Read a 16-byte page from the EEPROM."
        "\r\n\t8 - Write all HIGH to the EEPROM."
        "\r\n\t9 - Read full contents from the EEPROM."
        "\r\n\t0 - Check if the EEPROM is ready."
        "\r\n\tT - Write fake data."
        "\r\n\tR - Read fake data."
        );
}

void eeprom_write_test_single(){
    SerialUSB.println("Initiating test write single.");
    --counter;
    Address_Select address;
        address.block.segment = 0;
        address.block.data = counter;

    SerialUSB.print("Writing 0xFF to segment ");
        SerialUSB.print(address.block.segment);
        SerialUSB.print(" at address ");
        SerialUSB.print(address.block.data);
    if(write_to_eeprom(address, 0xAA)){
        SerialUSB.println(" succeeded!");
    } else {
        SerialUSB.println(" failed!");
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_read_test_single(){
    SerialUSB.println("Initiating test read.");
    SerialUSB.print("Byte read: ");
    Address_Select address;
        address.block.segment = 0;
        address.block.data = counter;
        SerialUSB.println(read_from_eeprom(address));
    SerialUSB.println("\tFinished running test.");
}

void eeprom_write_test(bool incremental){
    SerialUSB.println("Initiating test write multiple.");

    Address_Select address;

    byte to_write = 0xFF;
    for(
        address.block.segment = 0;
        address.block.segment < 2;
        ++address.block.segment
    ){
        address.block.data = 0x0;
        do{
            if(incremental) ++to_write;
            SerialUSB.print("Writing ");
                SerialUSB.print(to_write);
                SerialUSB.print(" to segment ");
                SerialUSB.print(address.block.segment);
                SerialUSB.print(" at address ");
                SerialUSB.print(address.block.data);
            if(write_to_eeprom(address, to_write)){
                SerialUSB.println(" succeeded!");
            } else {
                SerialUSB.println(" failed!");
            }
        } while(--address.block.data > 0);
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_read_test(){
    SerialUSB.println("Initiating test read multiple.");

    Address_Select address;

    for(
        address.block.segment = 0;
        address.block.segment < 2;
        ++address.block.segment
    ){
        address.block.data = 0x0;
        do{
            SerialUSB.print("Byte at segment: ");
                SerialUSB.print(address.block.segment);
                SerialUSB.print(" at address ");
                SerialUSB.print(address.block.data);
                SerialUSB.print(" is ");
                SerialUSB.println(read_from_eeprom(address));
        } while(--address.block.data > 0);
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_write_page_test(){
    SerialUSB.println("Initiating test page write.");

    Address_Select address;
        address.block.segment = 0;
        address.block.data = 0;
    byte towrite[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

    SerialUSB.print("Writing 16-byte page to EEPROM ");
    if(write_to_eeprom_str(address, towrite, 16)){
        SerialUSB.println(" succeeded!");
    } else {
        SerialUSB.println(" failed!");
    }
    
    SerialUSB.println("\tFinished running test.");
}

void eeprom_read_page_test(){
    SerialUSB.println("Initiating test page read.");

    Address_Select address;
        address.block.segment = 0;
        address.block.data = 0;
    byte toread[16];

    SerialUSB.print("Read ");
        SerialUSB.print(read_from_eeprom_str(address, toread, 16));
        SerialUSB.print(" bytes from EEPROM: ");
    for(size_type i = 0; i < 16; ++i){
        SerialUSB.print(toread[i]);
        SerialUSB.print(" ");
    }
    
    SerialUSB.println("\r\n\tFinished running test.");
}

#define BUF_TEST_WRITE_SIZE 4096
#define BUF_SECTION_COUNT 16

void eeprom_write_HIGH_page_test(){
    SerialUSB.println("Initiating test write all HIGH.");

    Address_Select address;

    byte buf[BUF_TEST_WRITE_SIZE];
    for(unsigned i = 0; i < BUF_TEST_WRITE_SIZE; ++i){
        buf[i] = 0xFF;
    }
    for(
        address.block.segment = 0;
        address.block.segment < 2;
        ++address.block.segment
    ){
        address.block.data = 0x0;
        for(
            unsigned i = 0;
            i < BUF_SECTION_COUNT;
            ++i, address.block.data += BUF_TEST_WRITE_SIZE
        ){
            while(eeprom_busy());   // Wait for the eeprom to finish writing operations

            SerialUSB.print("Writing ");
                SerialUSB.print(BUF_TEST_WRITE_SIZE);
                SerialUSB.print(" HIGH bytes starting on segment ");
                SerialUSB.print(address.block.segment);
                SerialUSB.print(" at address ");
                SerialUSB.print(address.block.data);
            if(write_to_eeprom_str(address, buf, BUF_TEST_WRITE_SIZE)){
                SerialUSB.println(" succeeded!");
            } else {
                SerialUSB.println(" failed!");
            }
        }
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_read_all_page_test(){
    SerialUSB.println("Initiating test read all.");

    Address_Select address;
        address.block.segment = 0;
        address.block.data = 0;
    byte toread[BUF_TEST_WRITE_SIZE];

    SerialUSB.print("EEPROM contents: ");
    for(size_type i = 0; i < BUF_SECTION_COUNT; ++i){
        read_from_eeprom_str(address, toread, BUF_TEST_WRITE_SIZE);
        for(size_type i = 0; i < BUF_TEST_WRITE_SIZE; ++i){
            SerialUSB.print(toread[i]);
            SerialUSB.print(" ");
        }
        SerialUSB.println("");
    }
    
    SerialUSB.println("\r\n\tFinished running test.");
}

void eeprom_write_incremental_page_test(){
    SerialUSB.println("Initiating test write all HIGH.");

    Address_Select address;

    byte buf[BUF_TEST_WRITE_SIZE];
    byte towrite = 0;
    for(unsigned i = 0; i < BUF_TEST_WRITE_SIZE; ++i, ++towrite){
        buf[i] = towrite;
    }
    for(
        address.block.segment = 0;
        address.block.segment < 2;
        ++address.block.segment
    ){
        address.block.data = 0x0;
        for(
            unsigned i = 0;
            i < BUF_SECTION_COUNT;
            ++i, address.block.data += BUF_TEST_WRITE_SIZE
        ){
            while(eeprom_busy());   // Wait for the eeprom to finish writing operations

            SerialUSB.print("Writing ");
                SerialUSB.print(BUF_TEST_WRITE_SIZE);
                SerialUSB.print(" HIGH bytes starting on segment ");
                SerialUSB.print(address.block.segment);
                SerialUSB.print(" at address ");
                SerialUSB.print(address.block.data);
            if(write_to_eeprom_str(address, buf, BUF_TEST_WRITE_SIZE)){
                SerialUSB.println(" succeeded!");
            } else {
                SerialUSB.println(" failed!");
            }
        }
    }
    SerialUSB.println("\tFinished running test.");
}

void eeprom_write_fake_data(){
    write_to_eeprom_str(fake_data_address, battery.reg, DATA_BYTE_SIZE);
    fake_data_address.block.data += DATA_BYTE_SIZE;
    ++battery.data.time;
}

void eeprom_read_last_fake_data(){
    Battery_Data hold;
    Address_Select addr_copy = fake_data_address;
    addr_copy.block.data -= DATA_BYTE_SIZE;
    read_from_eeprom_str(addr_copy, hold.reg, DATA_BYTE_SIZE);
    SerialUSB.print("Battery data:\r\n");
        SerialUSB.print("\tTime [s]: ");
        SerialUSB.println(hold.data.time);
        SerialUSB.print("\tState of Charge [%/bit]: ");
        SerialUSB.println(hold.data.SoC);
        SerialUSB.print("\tRemaining Capacity [mA/bit]: ");
        SerialUSB.println(hold.data.capacity);
        SerialUSB.print("\tVoltage [mV/bit]: ");
        SerialUSB.println(hold.data.volt);
        SerialUSB.print("\tCurrent [mA/bit]: ");
        SerialUSB.println(hold.data.curr);
        SerialUSB.print("\tTemperature [C]: ");
        SerialUSB.println(hold.data.temp);
        SerialUSB.print("\tEvent Code: ");
        SerialUSB.println(hold.data.event);
}