#include <fstream>
#include <iostream>
#include <limits>
#include <cstdint>
#include <string>

using dword = std::uint32_t;

    // Compact POD structure for measurement information
struct Battery_Data {
    dword
        time : 16,
        SoC : 8,
        capacity : 20,  // Note that by using dword, 1 byte will be
                        //  added as padding before capacity.
        volt : 12,
        curr : 12,
        temp : 12,
        event : 8
        ;
} interpreted;

int main(int argc, char* argv[]){
    if(argc < 3){
        std::cout << "Usage: battery_data_convert [SOURCE] [DESTINATION]\r\n";
        return 0;
    }

    using namespace std;

    ifstream src(argv[1]);
    ofstream dest(argv[2]);
    if(!src.is_open()){
        cerr << "Could not open " << argv[1] << endl;
        return 0;
    }
    if(!dest.is_open()){
        cerr << "Could not open " << argv[2] << endl;
        return 0;
    }

    src.ignore(numeric_limits<streamsize>::max(), ']');
    while(src.read(reinterpret_cast<char*>(&interpreted), 12)){
        dest
            << "Time: " << interpreted.time << std::endl
            << "State of charge: " << interpreted.SoC << std::endl
            << "Capacity: " << interpreted.capacity << std::endl
            << "Voltage: " << interpreted.volt << std::endl
            << "Current: " << interpreted.curr << std::endl
            << "Temperature: " << interpreted.temp << std::endl
            << "Event: " << interpreted.event << std::endl
            ;
        src.ignore(numeric_limits<streamsize>::max(), ']');
    }

    return 0;
}