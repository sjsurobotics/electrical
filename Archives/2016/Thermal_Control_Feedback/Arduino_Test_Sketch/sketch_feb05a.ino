
// I2C Digital Potentiometer
// by Nicholas Zambetti <http://www.zambetti.com>
// and Shawn Bonkowski <http://people.interaction-ivrea.it/s.bonkowski/>

// Demonstrates use of the Wire library
// Controls AD5171 digital potentiometer via I2C/TWI

// Created 31 March 2006

// This example code is in the public domain.

// This example code is in the public domain.


#include <Wire.h>

void setup() {
  Serial.begin(9600);
  Serial.println("Staring");
  Wire.begin(); // join i2c bus (address optional for master)

  readReg(0x68);
  writeReg(0x68, 0xFF);
  readReg(0x68);
  writeReg(0x68, 0x00);
  readReg(0x68);
  
  readReg(0x69);
  writeReg(0x69, 0xFF);
  readReg(0x69);
  writeReg(0x69, 0x00);
  readReg(0x69);

  writeReg(0x40, 0xC1);
  
  //writeReg(0x74, 0x00);
  
  writeReg(0x6A, 0x00);
  writeReg(0x6B, 0x00);
  writeReg(0x6C, 0x00);
  writeReg(0x6D, 0x00);

  writeReg(0x38, 0xFF);
  writeReg(0x39, 0xFF);
  writeReg(0x3A, 0xFF);
  writeReg(0x3B, 0xFF);

  readReg(0x6A);
  readReg(0x6B);
  readReg(0x6C);
  readReg(0x6D);

  readReg(0x38);
  readReg(0x39);
  readReg(0x3A);
  readReg(0x3B);

  //begin Temp 
}

void loop() {
  Serial.println("Full speed!");
  writeReg(0x32, 0x83);
  readReg(0x32);
  delay(1000);
  
  Serial.println("No speed!");
  writeReg(0x32, 0x00);
  readReg(0x32);
  delay(2000);

  // measure temp
  
 writeReg(0x40, 0xC1);
 delay(200);
 writeReg(0x40, 0x41);
 delay(50);
 readReg(0x20);
 readReg(0x21);
 readReg(0x22);
 readReg(0x23);
 readReg(0x24);
 readReg(0x25);
 readReg(0x26);
 readReg(0x27);
 readReg(0x28);
 readReg(0x29);
 writeReg(0x40, 0xC1);
}



void writeReg(byte addr, byte data) { 
  Wire.beginTransmission(0x2c); // transmit to device #44 (0x2C)
  // device address is specified in datasheet
  Wire.write(addr);            // sends instruction byte
  Wire.write(data);            // sends instruction byte
  Serial.print("\t\twriteReg(): Wire.endTransmission() = ");
  Serial.println(Wire.endTransmission());     // stop transmitting
}

void readReg(byte addr) {
    Wire.beginTransmission(0x2C);
    Wire.write(addr);
    Wire.endTransmission(false);
    int len = Wire.requestFrom(0x2C, 1);
    int cnt = 0;
    long before = millis();
    Serial.print("[0x");
    Serial.print(addr, HEX);
    Serial.print("] ");
    Serial.print("Returned: ");
    while ((cnt < len) && ((millis() - before) < 100))
    {
        if (Wire.available()) { Serial.print(Wire.read()*1.8+32, DEC); }
    }
    Serial.println("");
}
