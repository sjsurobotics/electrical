
/***********************************************
			Dependencies Start
***********************************************/

#include <math.h>
#include <Servo.h>

/***********************************************
			Dependencies End
***********************************************/

#define DEBUG 1
#define TACHOMETER 1

#define MODE_INPUT_ELEMENT_ARGS		1
#define VECTOR_INPUT_ELEMENT_ARGS	2
#define MODE_DEFAULT 				"K"
#define SPEED_DEFAULT				0
#define ANGLE_DEFAULT				90
#define NUMBER_OF_WHEELS			4

//Servo duty ratio timing constants (in microseconds)
#define SERVO_CENTER 				1500
#define SERVO_LIMIT_RIGHT			1950
#define SERVO_LIMIT_LEFT			1050

#define DIRECTION_RIGHT				true
#define DIRECTION_LEFT				false

typedef char tank_mode_rotato;
#define TANK_FORWARD				'F'
#define TANK_ROTATE					'R'

//MATH CONSTANTS
typedef float math_constants;
#define DEG_TO_RAD					.0174533
#define TUNING_CONSTANT				2.330033 //calculated by previous math *magic number.Ratio between W/L of the rover
#define RAD_TO_DEG					57.2958
#define REGRESSION_CONSTANT			0.000043573
#define MOTOR_ANGSKEW_1				0
#define MOTOR_ANGSKEW_2				0
#define MOTOR_ANGSKEW_3				10
#define MOTOR_ANGSKEW_4				10
#define SPEED_DAMP					0.08
#define ANGLE_DAMP					0.04

//Different Roving Modes
typedef char mode_types;
#define CRAB_MODE 'C'
#define CAR_MODE 'K'
#define TANK_MODE 'T'

//typedef int direction_types;
//#define DIRECTION_FORWARD			1
//#define DIRECTION_BACKWARD			0
/***********************************************
			Global Variables Start
***********************************************/

//PINOUT OF THE ROVER 
struct fistPins_t {
	const int WHEEL_SPEED_PWM[4] = {2, 3, 10, 5}; 
	const int WHEEL_DIRECTION_PIN[4] = {22, 24, 26, 28};
	const int WHEEL_ANGLE_PWM[4] = {6, 7, 8, 9};
	const int WHEEL_FEEDBACK_INT[4] = {18, 19, 20, 21};
} pins;

unsigned int DIRECTION_FORWARD = 1;
unsigned int DIRECTION_BACKWARD = 0;
//INPUT TYPES FROM ROVERCORE
struct Inputs_t {
	char mode = CAR_MODE;
	int speed = 0;
	int angle = 90;
} goals;

//OUTPUT TYPES TO MOTORS (Speed,Angle,Direction)
struct Outputs_t {
	float wheel_speed_output[4] = {0,0,0,0};
	float wheel_angle_output[4] = {90,90,90,90};
	float wheel_angle_output_micro[4] = {1500,1500,1500,1500};
	unsigned int rover_direction = 1;
	float mod_angle = 0;
} final, calc, last;

struct Modifiers_t {
	float mod_angle = 0;
	float mod_turn_angle = 0;
	float mod_speed = 0;
	char mod_tank_region = 'F';
} math;

struct State_t {
	unsigned long time;
	unsigned long dtime;
} states;

// When Facing Forward (configuration similar to a standard chip package)
Servo swerve1; //Top left
Servo swerve2; //Bottom left
Servo swerve3; //Bottom right
Servo swerve4; //Top right

String inputString = "";
char buffer[200] = {};
char statusbuffer[200] = {};

/***********************************************
			Global Variables End
***********************************************/

void setup() {
	Serial.begin(9600);
		inputString.reserve(200);
	  TCCR1B = (TCCR1B & 0b11111000) | 0x01;
	  TCCR2B = (TCCR2B & 0b11111000) | 0x01;
	  TCCR3B = (TCCR3B & 0b11111000) | 0x01;
	  TCCR4B = (TCCR4B & 0b11111000) | 0x01;
	for (int i = 0; i < 3; ++i) {
		pinMode(pins.WHEEL_SPEED_PWM[i], OUTPUT);
		pinMode(pins.WHEEL_DIRECTION_PIN[i], OUTPUT);
	}

	swerve1.attach(pins.WHEEL_ANGLE_PWM[0]);
	swerve2.attach(pins.WHEEL_ANGLE_PWM[1]);
	swerve3.attach(pins.WHEEL_ANGLE_PWM[2]);
	swerve4.attach(pins.WHEEL_ANGLE_PWM[3]);

	swerve1.writeMicroseconds(SERVO_CENTER);
	swerve2.writeMicroseconds(SERVO_CENTER);
	swerve3.writeMicroseconds(SERVO_CENTER);
	swerve4.writeMicroseconds(SERVO_CENTER);
	Serial.println("setup done");
	states.dtime = millis();
	states.time = millis();

	digitalWrite(pins.WHEEL_SPEED_PWM[0],LOW);
	digitalWrite(pins.WHEEL_SPEED_PWM[1],LOW);
	digitalWrite(pins.WHEEL_SPEED_PWM[2],HIGH);
	digitalWrite(pins.WHEEL_SPEED_PWM[3],HIGH);
}

boolean brake = true;
unsigned int saved_directon = 1;
float saved_speed = 0;

void loop() {
	checkInputs();
	
	if(goals.angle <= 180 && goals.angle >=0) { calc.rover_direction = 1;}
	if(goals.angle > 180 && goals.angle <= 360) { calc.rover_direction = 0;}
	if(!brake && final.rover_direction != calc.rover_direction) {
		saved_directon = calc.rover_direction;
		saved_speed = goals.speed;
		brake = true;
	} else if(brake && final.wheel_speed_output[0] > 5) {
		goals.speed = 0;

	} else if(brake && final.wheel_speed_output[0] <= 5) {
		final.rover_direction = saved_directon;
		goals.speed = saved_speed;
		brake = false;
	}

	switch(goals.mode) {
		case CAR_MODE:
			Car_Mode();
			break;
		case TANK_MODE:
			Tank_Mode();
			break;
		case CRAB_MODE:
			Crab_Mode();
			break;
		default:
			// NOTE: change it to stopped mode later
			goals.mode = CAR_MODE;
			break;
	}
	Motor_output();
	printWheelStatus();
}

bool serial_complete = false;

void Motor_output () { 
	int dir;
	if(millis()-states.dtime > 20) {

		for (int i = 0;i <= sizeof(final.wheel_angle_output_micro)/sizeof(float)-1;i++) { //Setting speed PWMs
			final.wheel_speed_output[i] = (SPEED_DAMP*calc.wheel_speed_output[i])+((1-SPEED_DAMP)*final.wheel_speed_output[i]);
			analogWrite(pins.WHEEL_SPEED_PWM[i],final.wheel_speed_output[i]);
			//if(abs(final.wheel_angle_output-last.wheel_angle_output) > 5){
			last.wheel_angle_output[i] = (ANGLE_DAMP*final.wheel_angle_output[i])+((1-ANGLE_DAMP)*last.wheel_angle_output[i]);
			final.wheel_angle_output_micro[i] = servoAngletoMicro(last.wheel_angle_output[i]);
		}
		if(goals.mode == CAR_MODE || goals.mode == CRAB_MODE){
		//Serial.println(final.rover_direction);

			for(int i = 0; i <= 1; i++) {
			 	if(final.rover_direction == 1) {
			 		digitalWrite(pins.WHEEL_DIRECTION_PIN[i],HIGH);
			 		digitalWrite(pins.WHEEL_DIRECTION_PIN[i+2],LOW);
		 		} else if(final.rover_direction == 0) {
			 		digitalWrite(pins.WHEEL_DIRECTION_PIN[i],LOW);
			 		digitalWrite(pins.WHEEL_DIRECTION_PIN[i+2],HIGH);
		 		}
			}


		//	dir = (final.rover_direction == DIRECTION_FORWARD) ? HIGH : LOW;
			//int right_dir = (final.rover_direction == DIRECTION_BACKWARD) ? LOW : HIGH;
		//	digitalWrite(pins.WHEEL_DIRECTION_PIN[0], dir);
		//	digitalWrite(pins.WHEEL_DIRECTION_PIN[1], dir);
		//	digitalWrite(pins.WHEEL_DIRECTION_PIN[2], ~dir & 0x1);
		//	digitalWrite(pins.WHEEL_DIRECTION_PIN[3], ~dir & 0x1);
		} else if(goals.mode == TANK_MODE){
			switch(math.mod_tank_region){
				case 'F':
					dir = (final.rover_direction == DIRECTION_FORWARD) ? HIGH : LOW;
					//int right_dir = (final.rover_direction == DIRECTION_BACKWARD) ? LOW : HIGH;

					digitalWrite(pins.WHEEL_DIRECTION_PIN[0], dir);
					digitalWrite(pins.WHEEL_DIRECTION_PIN[1], dir);
					digitalWrite(pins.WHEEL_DIRECTION_PIN[2], ~dir & 0x1);
					digitalWrite(pins.WHEEL_DIRECTION_PIN[3], ~dir & 0x1);
					break;
				case 'R':
					dir = (final.rover_direction == DIRECTION_LEFT) ? HIGH : LOW;
					//int right_dir = (final.rover_direction == DIRECTION_BACKWARD) ? LOW : HIGH;

					digitalWrite(pins.WHEEL_DIRECTION_PIN[0], dir);
					digitalWrite(pins.WHEEL_DIRECTION_PIN[1], dir);
					digitalWrite(pins.WHEEL_DIRECTION_PIN[2], dir);
					digitalWrite(pins.WHEEL_DIRECTION_PIN[3], dir);
					break;
				default:
					math.mod_tank_region = TANK_FORWARD;
					break;
			}
		}
		swerve1.writeMicroseconds(final.wheel_angle_output_micro[0] + MOTOR_ANGSKEW_1);
		swerve2.writeMicroseconds(final.wheel_angle_output_micro[1] + MOTOR_ANGSKEW_2);
		swerve3.writeMicroseconds(final.wheel_angle_output_micro[2] + MOTOR_ANGSKEW_3);
		swerve4.writeMicroseconds(final.wheel_angle_output_micro[3] + MOTOR_ANGSKEW_4);
		states.dtime = millis();
	}
}

void Crab_Mode() {
	if(goals.angle >= 0 && goals.angle <= 180){ 
		math.mod_turn_angle = goals.angle;
		final.rover_direction = DIRECTION_FORWARD;
	}

	if(goals.angle > 180 && goals.angle <=360){ 
		math.mod_turn_angle = 360 + goals.angle*(-1);
		final.rover_direction = DIRECTION_BACKWARD;
	}
	for(int i = 0; i<= 3; i++){
		final.wheel_angle_output[i] = math.mod_turn_angle;
		final.wheel_speed_output[i] = goals.speed;
	}
}

/*CAR MODE OPERATION
MODE INPUT: "MKE"
Car mode operation takes in input via USB Serial in this format "S(#1),(#2)E" with (#1) being Speed (0-255)
and (#2) being angle in degrees (0-360). 
*/
void Car_Mode() {

	//Serial.println(final.rover_direction);
	if(goals.angle <= 180 && goals.angle >= 0) { math.mod_angle = goals.angle; }
	if(goals.angle > 180 && goals.angle <= 360) { math.mod_angle = (goals.angle - 360)*(-1); }
	if(math.mod_angle != 90) { math.mod_turn_angle = (90 - math.mod_angle)/2; }
	if(math.mod_angle == 90) { math.mod_turn_angle = 0; }

	if(math.mod_turn_angle >= 5) { // RIGHT ALIGNED JOYSTICK
		calc.wheel_angle_output[3] = math.mod_turn_angle;
		calc.wheel_angle_output[0] = atan(1/((1/tan(math.mod_turn_angle*DEG_TO_RAD))+TUNING_CONSTANT))*RAD_TO_DEG;
		calc.wheel_angle_output[1] = 90 + calc.wheel_angle_output[0];
		calc.wheel_angle_output[2] = 90 + calc.wheel_angle_output[3];

		final.wheel_angle_output[0] = 90 - calc.wheel_angle_output[0];
		final.wheel_angle_output[1] = calc.wheel_angle_output[1];
		final.wheel_angle_output[2] = calc.wheel_angle_output[2];
		final.wheel_angle_output[3] = 90 - calc.wheel_angle_output[3];

		calc.wheel_speed_output[0] = goals.speed - (goals.speed*goals.speed*math.mod_turn_angle*REGRESSION_CONSTANT); //
		calc.wheel_speed_output[1] = calc.wheel_speed_output[0];
		calc.wheel_speed_output[2] = calc.wheel_speed_output[0]*((sin((calc.wheel_angle_output[0]+1)*DEG_TO_RAD))/(sin(((math.mod_turn_angle+1)*DEG_TO_RAD))));
		calc.wheel_speed_output[3] = calc.wheel_speed_output[2];
	} else if(math.mod_turn_angle < 5 && math.mod_turn_angle >= -5){ //CENTERED JOYSTICK
		for(int i = 0; i <=3; i++ ) {
			final.wheel_angle_output[i] = 90;
			calc.wheel_speed_output[i] = goals.speed;
		}
	} else if(math.mod_turn_angle < -5){ //LEFT ALIGNED JOYSTICK
		math.mod_turn_angle = math.mod_turn_angle*(-1);
		calc.wheel_angle_output[3] = math.mod_turn_angle;
		calc.wheel_angle_output[0] = atan(1/((1/tan(math.mod_turn_angle*DEG_TO_RAD))+TUNING_CONSTANT))*RAD_TO_DEG;
		calc.wheel_angle_output[1] = 90 + calc.wheel_angle_output[0];
		calc.wheel_angle_output[2] = 90 + calc.wheel_angle_output[3];

		final.wheel_angle_output[0] = calc.wheel_angle_output[2];
		final.wheel_angle_output[1] = 90 - calc.wheel_angle_output[3];
		final.wheel_angle_output[2] = 90 -calc.wheel_angle_output[0];
		final.wheel_angle_output[3] = calc.wheel_angle_output[1];

		calc.wheel_speed_output[2] = goals.speed - (goals.speed*goals.speed*math.mod_turn_angle*REGRESSION_CONSTANT); //(0.8*(exp((-1)*sin(math.mod_turn_angle*.DEG_TO_RAD))+.2))*
		calc.wheel_speed_output[0] = calc.wheel_speed_output[2]*((sin((calc.wheel_angle_output[0]+1)*DEG_TO_RAD))/(sin((math.mod_turn_angle+1)*DEG_TO_RAD)));
		calc.wheel_speed_output[1] = calc.wheel_speed_output[0];
		calc.wheel_speed_output[3] = calc.wheel_speed_output[2];
	}
}

/*TANK MODE OPERATION
MODE INPUT: "MTE"
-Tank mode operation is programmed to do four specific operations. It can go forward, backward, turn in place left,
and turn in place right. The angle and velocity inputs will be scaled depending on which operation (turning and backwards is
half speed and forward operation is full speed in comparison to the input)
*/
void Tank_Mode() {
	if(goals.angle <= 135 && goals.angle >= 45){ //TANK FORWARD
		math.mod_speed = goals.speed;
		math.mod_tank_region = TANK_FORWARD;
	}
	if(goals.angle < 45 || (goals.angle <=360 && goals.angle > 315)){ //TANK TURN IN PLACE RIGHT
		math.mod_speed = goals.speed/1.5;
		math.mod_tank_region = TANK_ROTATE;
		final.rover_direction = DIRECTION_RIGHT;
	}
	if(goals.angle > 135 && goals.angle <= 225){ //TANK TURN IN PLACE LEFT
		math.mod_speed = goals.speed/1.5;
		math.mod_tank_region = TANK_ROTATE;
		final.rover_direction = DIRECTION_LEFT;
	}
	if(goals.angle > 225 && goals.angle <= 315){ // TANK ROVE BACKWARD
		math.mod_speed = goals.speed/2;
		math.mod_tank_region = TANK_FORWARD;
		final.rover_direction = DIRECTION_BACKWARD;
	}
	for(int i = 0; i <=3; i++ ) {
		final.wheel_angle_output[i] = 90;
		calc.wheel_speed_output[i] = math.mod_speed;
	}
}

int servoAngletoMicro(int angle) {
	// needs to change to allow full 180 degrees
	angle = map(angle, 0, 180, SERVO_LIMIT_LEFT, SERVO_LIMIT_RIGHT);
	return angle; 
}

void serialEvent() {
	while(Serial.available()) {
		char input_Char = (char)Serial.read();
		inputString += input_Char;
		if(input_Char == '\n') {
			serial_complete = true;
			Serial.read();
		}
	}
}
void checkInputs() {
	if(serial_complete) {
		Inputs_t tmp;
		int string_counter = 0;
		int check_mode = 0;
		int check_vector = 0;
		const char * input_str = inputString.c_str();
		check_mode = sscanf(input_str, "M%cE", &tmp.mode);
		check_vector = sscanf(input_str,"S%d,%dE",
			&tmp.speed,
			&tmp.angle
		);
		Serial.println(input_str);
		sprintf(buffer, "Check Mode: %d, Check Vector = %d\n", check_mode, check_vector);
		Serial.println(buffer);

		if(check_mode == MODE_INPUT_ELEMENT_ARGS) {
			switch(tmp.mode) {
				case CAR_MODE:
					goals.mode = CAR_MODE;
					break;
				case CRAB_MODE: 
					goals.mode = CRAB_MODE;
					break;
				case TANK_MODE:
					goals.mode = TANK_MODE;
					break;
				default:
					Serial.println("FAILED TO SET MODE");
					break;
			}
			// Reset goal speed and angle 
			goals.speed = 0;
			goals.angle = 90;
			sprintf(buffer, "Mode change to %c\n", tmp.mode);
			Serial.print(buffer);
		}
		else if(check_vector == VECTOR_INPUT_ELEMENT_ARGS) {
			sprintf(buffer, "Speed = %d, Angle = %d\n", tmp.speed, tmp.angle);
			Serial.print(buffer);
			goals.speed = tmp.speed;
			goals.angle = tmp.angle;
		}
		else {
			Serial.println("sscanf failure");
		}
		serial_complete = false;
		inputString = "";
		Serial.flush();
	}
}


void printWheelStatus() {
	#if DEBUG
	if(millis()-states.time > 500) {
		for(int i = 0; i <=3 ; i++) {
			Serial.print(final.wheel_speed_output[i]);
			Serial.print(",");
			Serial.print(final.wheel_angle_output[i]);
			Serial.print('\t');
		}
		Serial.print(math.mod_turn_angle);
		Serial.print('\t');
		Serial.println(goals.mode);
		states.time = millis();
	}
	#endif
}