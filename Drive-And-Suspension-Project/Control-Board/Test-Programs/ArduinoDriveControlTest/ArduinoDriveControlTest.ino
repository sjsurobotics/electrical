/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int dir = 2;
int spd = 3;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

String mode = "";


// the setup routine runs once when you press reset:
void setup() {                
    pinMode(dir, OUTPUT);
    pinMode(spd, OUTPUT);
    // initialize serial:
    Serial.begin(9600);
    // reserve 200 bytes for the inputString:
    inputString.reserve(200);
}

// the loop routine runs over and over again forever:
void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {
    Serial.println(inputString);
    if (inputString == "dir\n")
    {
      mode = "dir";
      Serial.println("In dir mode");
    }
    else if (inputString == "spd\n")
    {
      mode = "spd";
      Serial.println("In spd mode");
    }
    else if (mode != "")
    {
      int inputInt = inputString.toInt();
      if (mode == "dir")
      {
        if (inputInt == 0)
        {
          digitalWrite(dir, LOW);
          Serial.println("Dir LOW");
        }
        else if (inputInt == 1)
        {
          digitalWrite(dir, HIGH);
          Serial.println("Dir High");
        }
        else
        {
          Serial.println("Number must be 0 or 1");
        }
      }
      if (mode == "spd")
      {
        if (inputInt >= 0 && inputInt <= 255)
        {
          analogWrite(spd, inputInt);
          Serial.println("Analog Write " + inputString);
        }
        else
        {
          Serial.println("Number must be in between 0 and 255");
        }
      }
    }
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
