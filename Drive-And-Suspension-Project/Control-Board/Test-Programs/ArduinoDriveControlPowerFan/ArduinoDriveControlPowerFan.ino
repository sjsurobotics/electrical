/*
  AnalogReadSerial
 Reads an analog input on pin 0, prints the result to the serial monitor.
 Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.
 
 This example code is in the public domain.
 */
int out = 2;
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete



// the setup routine runs once when you press reset:
void setup() {
  pinMode(out, OUTPUT);
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
}

// the loop routine runs over and over again forever:
void loop() {
  if (stringComplete) {
    Serial.println(inputString);
    if (inputString == "on\n")
    {
      digitalWrite(out, HIGH);
    }
    if (inputString == "off\n")
    {
      digitalWrite(out, LOW);
    }
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

