/*
  AnalogReadSerial
 Reads an analog input on pin 0, prints the result to the serial monitor.
 Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.
 
 This example code is in the public domain.
 */
int s0 = 2;
int s1 = 3;
int s2 = 4;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete



// the setup routine runs once when you press reset:
void setup() {
  pinMode(s0, OUTPUT);
  pinMode(s1, OUTPUT);
  pinMode(s2, OUTPUT);
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
}

// the loop routine runs over and over again forever:
void loop() {
  if (stringComplete) {
    Serial.println(inputString);
    if (inputString == "measure\n")
    {
      // read the input on analog pin 0:
      int sensorValue = analogRead(A1);
      float voltage = sensorValue * (5.0 / 1023.0);
      // print out the value you read:
      Serial.println(sensorValue);
      Serial.println(voltage);
    }
    else
    {
      int inputInt = inputString.toInt();
      if (inputInt % 2 == 1)
      {
        digitalWrite(s0, HIGH);
        Serial.println("S0 HIGH");
      }
      else
      {
        digitalWrite(s0, LOW);
        Serial.println("S0 LOW");
      }
      if ((inputInt / 2) % 2 == 1)
      {
        digitalWrite(s1, HIGH);
        Serial.println("S1 HIGH");
      }
      else
      {
        digitalWrite(s1, LOW);
        Serial.println("S1 LOW");
      }
      if ((inputInt / 4) % 2 == 1)
      {
        digitalWrite(s2, HIGH);
        Serial.println("S2 HIGH");
      }
      else
      {
        digitalWrite(s2, LOW);
        Serial.println("S2 LOW");
      }
    }
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

